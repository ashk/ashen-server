/*
* Ashen Core
*/

#ifndef FREE_FOR_ALL_MGR_H_
#define FREE_FOR_ALL_MGR_H_

#include <ace/Singleton.h>
#include "FreeForAll.h"
#include "Player.h"

#define FFA_TIMER_CHANGER 1*MINUTE*IN_MILLISECONDS

class FreeForAllMgr
{

public:
    FreeForAllMgr(); //Constructeur
    ~FreeForAllMgr(); //Deconstruceur

    bool LoadFreeForAll(); //Chargement des FreeForAll
    void tickRandomFreeForAll(); //Tick pour generer une zone FFA Al�atoire
    bool activeFreeForAll(uint32 id); //Active la free for all d�finis pas l'id, d�active l'active
    bool isFreeForAll(uint32 id); //V�rifie si l'id existe
    FreeForAll* getActiveFreeForAll() { return this->activedFreeForAll; }; //Retourne la FFA Active
    std::map<uint32, FreeForAll*> getFreeForAllInstance() { return this->FreeForAllInstance; };
    uint32 countFreeForAll() { return this->FreeForAllInstance.size(); };
    uint32 countPlayersFFA() { return this->players.size(); };

    void addPlayerInFFA(Player* player);
    void removePlayerInFFA(Player* player);
    void teleportInFFA(Player* player);

    uint32 ffazonetimer;

private:
    std::map<uint32, FreeForAll*> FreeForAllInstance;
    FreeForAll* activedFreeForAll; //FreeForAll Active 
    std::vector<Player*> players; //L'ensemble des players en zoneFFA, au changement tout les joueurs sont tp a une entry al�atoire

    uint32 getRandomFFA(); //Selectionne un numeros al�atoire dans les instances
    FreeForAll* getFFA(uint32 id); //Retourne l'objet associ�e a id, sinon Null

    //Loader s�par�
    bool LoadFreeForAllTemplate();
    bool LoadFreeForAllSpawnCreature();
    bool LoadFreeForAllSpawnGameobject();
    bool LoadFreeForAllSpawnPlayer();
    bool LoadFreeForAllModule();
};

#define sFreeForAllMgr ACE_Singleton<FreeForAllMgr, ACE_Null_Mutex>::instance()
#endif // FREE_FOR_ALL_MGR_H_
