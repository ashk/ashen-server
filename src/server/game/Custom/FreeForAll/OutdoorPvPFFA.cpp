/*
* Ashen Core
*/

#include "SpellAuras.h"
#include "Vehicle.h"
#include "ObjectMgr.h"
#include "World.h"
#include "Chat.h"
#include "MapManager.h"
#include "ScriptedCreature.h"
#include "ScriptPCH.h"
#include "Group.h"
#include "Item.h"
#include "OutdoorPvPFFA.h"
#include "FreeForAllMgr.h"
#include "FreeForAll.h"
#include "DBCStores.h"
#include "KillingStreak.h"

OutdoorPvPFFA::OutdoorPvPFFA()
{
        m_TypeId = OUTDOOR_PVP_FFA;
}

bool OutdoorPvPFFA::SetupOutdoorPvP()
{
    TC_LOG_INFO("server.loading", "[FreeForAll] System Init");

    std::map<uint32, FreeForAll*> freeForAllInstance = sFreeForAllMgr->getFreeForAllInstance();
    for (std::map<uint32, FreeForAll*>::iterator itr = freeForAllInstance.begin(); itr != freeForAllInstance.end(); ++itr)
    {
        std::vector<uint32> areaIdList = itr->second->getAreaId();
        for (std::vector<uint32>::iterator itrArea = areaIdList.begin(); itrArea != areaIdList.end(); ++itrArea)
        {
            if (AreaTableEntry const* area = GetAreaEntryByAreaID(*itrArea))
            {
                uint32 zoneId = area->zone;

                if (zoneId == 0)
                    zoneId = area->ID;

                TC_LOG_INFO("server.loading", "[FreeForAll] Zone %u register in FFA", zoneId);
                this->RegisterZone(zoneId);

            }  
        }
    }

    sFreeForAllMgr->ffazonetimer = FFA_TIMER_CHANGER;

    //Aucune ffa on skip
    if (sFreeForAllMgr->countFreeForAll() <= 0)
        return true;

    if (sFreeForAllMgr->countFreeForAll() > 1) //Plus d'une FFA on active l'al�atoire
        sFreeForAllMgr->tickRandomFreeForAll(); 
    else //Une seule ffa on active la premiere
    {
        sFreeForAllMgr->activeFreeForAll(sFreeForAllMgr->getFreeForAllInstance().begin()->first);
    }

    return true;
}

void OutdoorPvPFFA::HandlePlayerEnterZone(Player* plr, uint32 zoneId)
{
    TC_LOG_INFO("server.loading", "[FreeForAll] %s enter in FFA Zone", plr->GetName().c_str());
    sFreeForAllMgr->addPlayerInFFA(plr);
    sKillingStreak->initPlayer(plr->GetGUID());

	OutdoorPvP::HandlePlayerEnterZone(plr, zoneId);
}

void OutdoorPvPFFA::HandlePlayerLeaveZone(Player* plr, uint32 zoneId)
{
    TC_LOG_INFO("server.loading", "[FreeForAll] %s leave FFA Zone", plr->GetName().c_str());
    sFreeForAllMgr->removePlayerInFFA(plr);	
    sKillingStreak->resetKill(plr->GetGUIDLow());

	OutdoorPvP::HandlePlayerLeaveZone(plr, zoneId);
}

void OutdoorPvPFFA::HandleKill(Player* plr, Unit* killed)
{
    TC_LOG_INFO("server.loading", "[FreeForAll] %s kill a Unit", plr->GetName().c_str());

    if (sKillingStreak->haveRequiredCondition() && sKillingStreak->isValidKill(plr->GetGUIDLow(), killed->GetGUIDLow()))
    {
        sKillingStreak->handleKill(plr->GetGUIDLow());
        sKillingStreak->handleKill(killed->GetGUIDLow(), true);
    }
}

bool OutdoorPvPFFA::Update(uint32 diff)
{
    //Le tick de la zone FFA n'est pas active si il n'y a q'une ffa
    if (sFreeForAllMgr->countFreeForAll() > 1)
    {
        //Timer pour changer de zone FFA
        if (sFreeForAllMgr->ffazonetimer <= diff)
        {
            sFreeForAllMgr->tickRandomFreeForAll();
            sFreeForAllMgr->ffazonetimer = FFA_TIMER_CHANGER;
        }
        else
            sFreeForAllMgr->ffazonetimer -= diff;
    }
  
	return true;
}

bool OutdoorPvPFFA::HandleCustomSpell(Player *plr, uint32 spellId, GameObject *go)
{
	return false;
}

class OutdoorPvP_ffa: public OutdoorPvPScript
{
    public:
        OutdoorPvP_ffa()
            : OutdoorPvPScript("outdoorpvp_ffa")
        {
        }

        OutdoorPvP* GetOutdoorPvP() const
        {
            return new OutdoorPvPFFA();
        }
};

void AddSC_outdoorpvp_ffa()
{
    new OutdoorPvP_ffa();
}