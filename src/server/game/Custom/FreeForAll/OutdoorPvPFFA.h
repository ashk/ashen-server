/*
Ashen Core
*/

#ifndef OUTDOOR_PVP_FFA_
#define OUTDOOR_PVP_FFA_

#include "OutdoorPvP.h"

class OutdoorPvPFFA : public OutdoorPvP
{
	public:
		OutdoorPvPFFA();

		void HandlePlayerEnterZone(Player* plr, uint32 zoneId);
		void HandlePlayerLeaveZone(Player* plr, uint32 zoneId);

		void HandleKill(Player* player, Unit* killed);

		bool HandleCustomSpell(Player *plr, uint32 spellId, GameObject *go);
		bool Update(uint32 diff);
		bool SetupOutdoorPvP();

	protected:

	private:
};

#endif