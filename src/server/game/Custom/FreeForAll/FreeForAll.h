/*
* Ashen Core
*/

#ifndef FREE_FOR_ALL_H_
#define FREE_FOR_ALL_H_

struct FreeForAllPlayerSpawn
{
    uint32 mapId;
    float position_x;
    float position_y;
    float position_z;
    float orientation;
};

enum FreeForAllState
{
    FREE_FOR_ALL_STATE_DISABLED,
    FREE_FOR_ALL_STATE_ENABLED,
};

class FreeForAll
{

private:
    std::vector<FreeForAllPlayerSpawn> playerSpawns; //Endroit o� les joueurs spawns
    std::vector<int32> gameobjectGuidList; //Liste des guid gameobjects
    std::vector<int32> creatureGuidList; //Liste des guid creatures
    std::vector<uint32> areaIdList; //Liste des areaId de la zone
    std::string name; //Nom de la zone
    uint32 id;

    //M�thodes
    void spawnEntity(bool remove = false); //Spawn des cr�atures et des gamobjects
    void spawnGameObjectEntity(bool remove = false); //Spawn des Gameobjects
    void spawnCreatureEntity(bool remove = false); //Spawn des Creature

public:
    FreeForAll(uint32 id, std::string name); //Constructeur
    ~FreeForAll(); //Deconstruceur

    uint32 getId();
    std::vector<uint32> getAreaId(){ return this->areaIdList; };
    std::string getName(){ return this->name; };
    std::vector<FreeForAllPlayerSpawn> getPlayerSpawn(){ return this->playerSpawns; };
    uint32 getPlayerSpawnCount();

    void setStateFFA(FreeForAllState state); //Active - Deactive la FFA
    FreeForAllPlayerSpawn getRandomSpawn(); //Retourne une cordonn�e al�atoire
    bool isFreeForAllArea(uint32 areaId);

    void addPlayerSpawns(FreeForAllPlayerSpawn spawn); //Ajout d'un spawn aleatoire
    void addGameobjectGuid(int32 guid); // Ajout d'un gameobject
    void addCreatureGuid(int32 guid); //Ajout d'une creature
    void addAreaId(uint32 areaId); //Ajout d'une area
};


#endif // FREE_FOR_ALL_H_
