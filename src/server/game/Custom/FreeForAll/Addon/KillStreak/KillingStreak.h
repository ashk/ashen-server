/*
* Ashen Core
*/

#ifndef KILLINGSTREAK_H_
#define KILLINGSTREAK_H_

#include <ace/Singleton.h>
#include "Player.h"

struct KillingStreakData
{
    int32 kill;
    uint32 lastGuidKilled;
};

struct KillingStreakReward
{
    uint32 honor;
    uint32 arena;
    uint32 itemId;
    uint32 itemQuantity;
    uint32 soundId;
    std::string announce;
};


#define KILLSTREAK_MIN_PLAYER 1

class KillingStreak
{

public:
    KillingStreak(); //Constructeur
    ~KillingStreak(); //Deconstruceur

    bool loadKillingStreak();
    void reset(); //Efface tout les killingstreaks

    void handleKill(uint32 guid, bool negative = false); //Rajoute un kill du guid
    void resetKill(uint32 guid); //Reset les kill du guid

    bool isValidKill(uint32 lowGUIDKiller, uint32 lowGUIDVictim); //Valide le kill
    bool haveRequiredCondition(); //V�rifie que la killstreak peut �tre lance
    void initPlayer(uint32 lowGUID);
 
    void giveReward(uint32 playerlowGUID, int32 killingStreakRewardId); //Donne les r�compenses

private:
    std::map<uint32, KillingStreakData> killingStreakPlayer;
    std::map<int32, KillingStreakReward> killingStreakReward;
  
};

#define sKillingStreak ACE_Singleton<KillingStreak, ACE_Null_Mutex>::instance()
#endif // KILLINGSTREAK_H_
