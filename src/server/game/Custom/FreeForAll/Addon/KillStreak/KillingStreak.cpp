/*
* Ashen Core
*/

#include "KillingStreak.h"
#include "FreeForAllMgr.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "Chat.h"
#include "DBCStores.h"

KillingStreak::KillingStreak(){};
KillingStreak::~KillingStreak(){};

bool KillingStreak::loadKillingStreak()
{
    uint32 oldMSTime = getMSTime();
    this->killingStreakReward.clear();

    QueryResult result = WorldDatabase.Query("SELECT entry, nb_kill, rew_honor, rew_arena, rew_item, count, sound_id, announce FROM ffa_killingstreak");

    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 FFA KillingStreak. DB table `ffa_killingstreak` is empty.");
        return false;
    }

    uint32 count = 0;
    do
    {
        Field *fields = result->Fetch();

        uint32 id = fields[0].GetUInt32();
        int32 kill = fields[1].GetInt32();
        uint32 honor = fields[2].GetUInt32();
        uint32 arena = fields[3].GetUInt32();
        uint32 itemId = fields[4].GetUInt32();
        uint32 quantity = fields[5].GetUInt32();
        uint32 soundId = fields[6].GetUInt32();
        std::string announce = fields[7].GetString();

        if (!sObjectMgr->GetItemTemplate(itemId)) // Item n'existe pas
        {
            TC_LOG_ERROR("server.loading", ">>   KillingStreak %u have invalide item id (%u) ", id, itemId);
            continue;
        }
        else if (itemId != 0 && quantity == 0)  // Quantite a zero
        {
            TC_LOG_ERROR("server.loading", ">>   KillingStreak %u have invalide item quantity (0) ", id);
            continue;
        }

        // SoundId n'existe pas
        if (!sSoundEntriesStore.LookupEntry(soundId) && soundId != 0)
        {
            TC_LOG_ERROR("server.loading", ">>   KillingStreak %u have invalide sound entry (%u) ", id, soundId);
            continue;
        }

        KillingStreakReward reward;
        reward.announce = announce;
        reward.arena = arena;
        reward.honor = honor;
        reward.itemId = itemId;
        reward.itemQuantity = quantity;
        reward.soundId = soundId;

        this->killingStreakReward.insert(std::pair<int32, KillingStreakReward>(kill, reward));
        count++;

    } while (result->NextRow());


    TC_LOG_INFO("server.loading", ">> Loaded %u KillingStreak %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    return true;
};


void KillingStreak::reset()
{
    this->killingStreakPlayer.clear();
};


void KillingStreak::initPlayer(uint32 guid)
{
    if (this->killingStreakPlayer.find(guid) == this->killingStreakPlayer.end())
    {
        KillingStreakData data;
        data.lastGuidKilled = 0;
        data.kill = 0;

        this->killingStreakPlayer.insert(std::pair<uint32, KillingStreakData>(guid, data));
    }
};

void KillingStreak::handleKill(uint32 guid, bool negative)
{
    if (this->killingStreakPlayer.find(guid) != this->killingStreakPlayer.end())
    {
        std::map<uint32, KillingStreakData>::iterator itr = this->killingStreakPlayer.find(guid);
        
        if (negative)
        {
            this->resetKill(guid);
            itr->second.kill--;
        }
        else
        {
            if (itr->second.kill < 0) itr->second.kill = 0;
            itr->second.kill++;
        }
            

        //Donne la récompense
        this->giveReward(guid, itr->second.kill);
    }
    else
        TC_LOG_ERROR("server.loading", "KillingStreak player %u not init", guid);
};

void KillingStreak::resetKill(uint32 lowGUID)
{
    if (this->killingStreakPlayer.find(lowGUID) != this->killingStreakPlayer.end())
    {
        std::map<uint32, KillingStreakData>::iterator itr = this->killingStreakPlayer.find(lowGUID);

        if (itr->second.kill > 0)
        {
            itr->second.kill = 0;
        }
        itr->second.lastGuidKilled = 0;
    }
};

bool KillingStreak::isValidKill(uint32 lowGUIDKiller, uint32 logGUIDVictim)
{
    /* 
   
    if (this->killingStreakPlayer.find(lowGUIDKiller) != this->killingStreakPlayer.end())
    {
        std::map<uint32, KillingStreakData>::iterator itr = this->killingStreakPlayer.find(logGUIDVictim);

        //Chain Kill
        if (itr->second.lastGuidKilled == logGUIDVictim)
            return false;
            
    }

    itr->second.lastGuidKilled = logGUIDVictim; 
    
    */
    return true;
};

bool KillingStreak::haveRequiredCondition()
{
    if (sFreeForAllMgr->countPlayersFFA() >= KILLSTREAK_MIN_PLAYER)
        return true;

    return false;
};

void KillingStreak::giveReward(uint32 playerLowGUID, int32 killingStreakRewardId)
{
    if (this->killingStreakReward.find(killingStreakRewardId) != this->killingStreakReward.end())
    {
        KillingStreakReward reward = this->killingStreakReward.find(killingStreakRewardId)->second;

        Player* player = sObjectMgr->GetPlayerByLowGUID(playerLowGUID);
        if (player)
        {
            //Reward Honor Point
            if (reward.honor > 0)
            {
                WorldPacket data(SMSG_PVP_CREDIT, 4 + 8 + 4);
                data << uint32(reward.honor);
                data << uint64(0);
                data << uint32(0);

                player->GetSession()->SendPacket(&data);
                player->ModifyHonorPoints((int32)reward.honor);
            }

            //Reward Arena Point
            if (reward.arena > 0)
            {
                player->ModifyArenaPoints((int32)reward.arena);
            }

            //Reward Item
            if (sObjectMgr->GetItemTemplate(reward.itemId))
            {
                player->AddItem(reward.itemId, reward.itemQuantity);
            }

            WorldPacket datamsg(SMSG_NOTIFICATION, (reward.announce.size() + 1));
            datamsg << reward.announce;

            sWorld->SendZoneMessage(player->GetZoneId(), &datamsg);
        }
    }
};