/*
* Ashen Core
*/

#include "FreeForAllMgr.h"
#include "FreeForAll.h"
#include "DBCStores.h"
#include "KillingStreak.h"

FreeForAllMgr::FreeForAllMgr()
{ 
    this->activedFreeForAll = NULL; 
};

FreeForAllMgr::~FreeForAllMgr(){};

bool FreeForAllMgr::LoadFreeForAll()
{
    this->LoadFreeForAllTemplate(); //Toujours charger les templates avant
    this->LoadFreeForAllSpawnCreature();
    this->LoadFreeForAllSpawnGameobject();
    this->LoadFreeForAllSpawnPlayer();
    this->LoadFreeForAllModule();

    return true;
};


/***************  LOADER */

bool FreeForAllMgr::LoadFreeForAllTemplate()
{
    uint32 oldMSTime = getMSTime();
    this->FreeForAllInstance.clear();

    QueryResult result = WorldDatabase.Query("SELECT entry, name, areaId FROM ffa_template");

    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 FFA Template. DB table `ffa_template` is empty.");
        return false;
    }

    uint32 count = 0;
    do
    {
        Field *fields = result->Fetch();

        uint32 id = fields[0].GetUInt32();
        std::string name = fields[1].GetString();
        std::string areaIdString = fields[2].GetString();

        FreeForAll* ffa = new FreeForAll(id, name);

        //-- Gestion des areas
        char* areaIdArgs = strtok((char*)areaIdString.c_str(), ";");

        if (areaIdArgs == NULL || atoi(areaIdArgs) == 0)
        {
            TC_LOG_ERROR("server.loading", ">>  No Area for FFA Template %u Skipped", id);
            continue;
        }
        
        do
        {
            uint32 areaId = atoi(areaIdArgs); //Conversion en nombre
            if (GetAreaEntryByAreaID(areaId)) //L'area existe
            {
                ffa->addAreaId(areaId);
                TC_LOG_ERROR("server.loading", ">>  Area %u added for FFA Template %u", areaId, id);
            }
            else
            {
                TC_LOG_ERROR("server.loading", ">>  Area %u is invalid area for FFA Template %u", areaId, id);
            }
            areaIdArgs = strtok(NULL, ";");
        } while (areaIdArgs != NULL);

        this->FreeForAllInstance.insert(std::pair<uint32, FreeForAll*>(id, ffa));
        count++;

    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u FFA definitions in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    return true;
};

bool FreeForAllMgr::LoadFreeForAllSpawnCreature()
{   
    uint32 oldMSTime = getMSTime();
    QueryResult result = WorldDatabase.Query("SELECT ffa_entry, creature_guid FROM ffa_creature");

    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 FFA Creature. DB table `ffa_creature` is empty.");
        return false;
    }

    uint32 count = 0;
    do
    {
        Field *fields = result->Fetch();

        uint32 ffa_entry = fields[0].GetUInt32();
        int32 creature_guid = fields[1].GetInt32();

        if (!this->isFreeForAll(ffa_entry))
        {
            TC_LOG_ERROR("server.loading", ">>  Invalid FFA Template %u - Skipped", ffa_entry);
            continue;
        }
           
        if (!sObjectMgr->GetCreatureData(abs(creature_guid)))
        {
            TC_LOG_ERROR("server.loading", ">>  Invalid Creature GUID %u for FFA Template %u - Skipped",  creature_guid, ffa_entry);
            continue;
        }

        FreeForAll* ffa = this->getFFA(ffa_entry);
        ffa->addCreatureGuid(creature_guid);
  
        count++;

    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u FFA Creature definitions in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    return true;
};

bool FreeForAllMgr::LoadFreeForAllSpawnGameobject()
{
    uint32 oldMSTime = getMSTime();
    QueryResult result = WorldDatabase.Query("SELECT ffa_entry, gob_guid FROM ffa_gob");

    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 FFA Gameobject. DB table `ffa_gob` is empty.");
        return false;
    }

    uint32 count = 0;
    do
    {
        Field *fields = result->Fetch();

        uint32 ffa_entry = fields[0].GetUInt32();
        int32 gameobject_guid = fields[1].GetInt32();

        if (!this->isFreeForAll(ffa_entry))
        {
            TC_LOG_ERROR("server.loading", ">>[FreeForAll] Invalid FFA Template %u - Skipped", ffa_entry);
            continue;
        }

        if (!sObjectMgr->GetGOData(abs(gameobject_guid)))
        {
            TC_LOG_ERROR("server.loading", ">>[FreeForAll] Invalid Gameobject GUID %u for FFA Template %u - Skipped", gameobject_guid, ffa_entry);
            continue;
        }

        FreeForAll* ffa = this->getFFA(ffa_entry);
        ffa->addGameobjectGuid(gameobject_guid);

        count++;

    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">>[FreeForAll] Loaded %u FFA Gameobject definitions in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    return true;
};

bool FreeForAllMgr::LoadFreeForAllSpawnPlayer()
{
    uint32 oldMSTime = getMSTime();
    QueryResult result = WorldDatabase.Query("SELECT entry, ffa_entry, mapId, position_x, position_y, position_z, orientation FROM ffa_player_spawn");

    if (!result)
    {
        TC_LOG_INFO("server.loading", ">>[FreeForAll] Loaded 0 FFA Spawn. DB table `ffa_player_spawn` is empty.");
        return false;
    }

    uint32 count = 0;

    do
    {
        Field *fields = result->Fetch();

        uint32 id = fields[0].GetUInt32();
        uint32 ffa_entry = fields[1].GetUInt32();
        uint32 mapId = fields[2].GetUInt32();
        float position_x = fields[3].GetFloat();
        float position_y = fields[4].GetFloat();
        float position_z = fields[5].GetFloat();
        float orientation = fields[6].GetFloat();

        if (!this->isFreeForAll(ffa_entry))
        {
            TC_LOG_ERROR("server.loading", ">>[FreeForAll] Invalid FFA Template %u for Entry %u - Skipped", ffa_entry, id);
            continue;
        }

        if (!MapManager::IsValidMapCoord(mapId, position_x, position_y, position_z, orientation))
        {
            TC_LOG_ERROR("server.loading", ">>[FreeForAll] Invalid FFA Spawn Coord %u - Skipped", id);
            continue;
        }

        FreeForAllPlayerSpawn spawn;
        spawn.mapId = mapId;
        spawn.position_x = position_x;
        spawn.position_y = position_y;
        spawn.position_z = position_z;
        spawn.orientation = orientation;

        FreeForAll* ffa = this->getFFA(ffa_entry);
        ffa->addPlayerSpawns(spawn);

        count++;

    } while (result->NextRow());


    TC_LOG_INFO("server.loading", ">>[FreeForAll] Loaded %u FFA Spawn Coord in %u ms", count, GetMSTimeDiffToNow(oldMSTime));

    return true;
};

//Chargement des modules Free For All
bool FreeForAllMgr::LoadFreeForAllModule()
{
    //Module Killing Streak
    sKillingStreak->loadKillingStreak();

    return true;
}

/*************** METHODES */

FreeForAll* FreeForAllMgr::getFFA(uint32 id)
{
    if (this->FreeForAllInstance.find(id) != this->FreeForAllInstance.end())
        return this->FreeForAllInstance.at(id); //Retourne le pointeur vers l'objet

    return NULL;
}

void FreeForAllMgr::tickRandomFreeForAll()
{
    //Il n'y a qu'une seule zone on ne peut pas faire le random
    if (this->countFreeForAll() <= 1)
    {
        TC_LOG_ERROR("server.loading", "[FreeForAll] Tick Random FFA Impossible");
        return;
    }

    TC_LOG_INFO("server.loading", "[FreeForAll] Tick Random FFA");
    uint32 activeZoneId = 0;
    uint32 nextFreeForAll = 0;

    if (this->activedFreeForAll) 
        activeZoneId = this->activedFreeForAll->getId();

    //Nouvelle FFA
    //On exclue l'actuel pour eviter deux fois la m�me zone
    // Ce tick n'est jamais appeler si il y a pas assez de zone ffa
    do
    {
        nextFreeForAll = urand(1, this->countFreeForAll());
    } while (nextFreeForAll == activeZoneId && activeZoneId != 0 && this->getFFA(nextFreeForAll) && this->getFFA(nextFreeForAll)->getPlayerSpawnCount() == 0);
    
    //Nouvelle zone - Activation
    this->activeFreeForAll(nextFreeForAll);

    //T�l�portation des joueurs
    //ver la nouvelle zone
    for (std::vector<Player*>::iterator itr = this->players.begin(); itr != this->players.end(); ++itr)
    {
        this->teleportInFFA(*itr);
    }
};

bool FreeForAllMgr::activeFreeForAll(uint32 id)
{
    TC_LOG_INFO("server.loading", "[FreeForAll] activation FFA Zone (%u)", id);

    //FFA id correcte
    if (this->isFreeForAll(id) && this->getFFA(id)->getPlayerSpawnCount() > 0)
    {
        //Si une zone est d�j� active on la d�active
        if (this->activedFreeForAll)
        {
            this->activedFreeForAll->setStateFFA(FREE_FOR_ALL_STATE_DISABLED); //D�activation
        }

        //On active la nouvelle
        this->activedFreeForAll = this->getFFA(id);
        this->activedFreeForAll->setStateFFA(FREE_FOR_ALL_STATE_ENABLED); //Activation
        return true;
    }

    //Erreur d'id
    return false;
}

bool FreeForAllMgr::isFreeForAll(uint32 id)
{
    return  this->FreeForAllInstance.find(id) != this->FreeForAllInstance.end();
}

void FreeForAllMgr::addPlayerInFFA(Player* player)
{
    if (player == NULL)
        return;

    //Joueur d�j� dans le tableau
    if (std::find(this->players.begin(), this->players.end(), player) != this->players.end())
        return;

    this->players.push_back(player);
}

void FreeForAllMgr::removePlayerInFFA(Player* player)
{
    if (player == NULL)
        return;

    //Dans le tableau
    std::vector<Player*>::iterator itr = std::find(this->players.begin(), this->players.end(), player);
    if (itr != this->players.end())
    {
        this->players.erase(itr);
    }
}

void FreeForAllMgr::teleportInFFA(Player* player)
{
    if (player && this->getActiveFreeForAll() && this->getActiveFreeForAll()->getPlayerSpawnCount() > 0)
    {
        TC_LOG_INFO("server.loading", "[FreeForAll] Teleport player %s in FFA", player->GetName().c_str());
        FreeForAllPlayerSpawn spawn = this->getActiveFreeForAll()->getRandomSpawn();
        player->TeleportTo(spawn.mapId, spawn.position_x, spawn.position_y, spawn.position_z, spawn.orientation);
    }
}