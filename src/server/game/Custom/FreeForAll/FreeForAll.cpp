/*
* Ashen Core
*/

#include "FreeForAll.h"

FreeForAll::FreeForAll(uint32 id, std::string name)
{
    this->id = id;
    this->name = name;
};

FreeForAll::~FreeForAll(){};

uint32 FreeForAll::getId()
{
    return this->id;
}

//PlayerSpawn
void FreeForAll::addPlayerSpawns(FreeForAllPlayerSpawn spawn)
{
    this->playerSpawns.push_back(spawn);
};

//Gameobject
void FreeForAll::addGameobjectGuid(int32 guid)
{
    this->gameobjectGuidList.push_back(guid);
};

//Creature
void FreeForAll::addCreatureGuid(int32 guid)
{
    this->creatureGuidList.push_back(guid);
};

//AreaId
void FreeForAll::addAreaId(uint32 areaId)
{
    this->areaIdList.push_back(areaId);
};

//GetRandomSpawn
FreeForAllPlayerSpawn FreeForAll::getRandomSpawn()
{
    uint32 size = this->getPlayerSpawnCount();

    //il n'y a qu'une téléportation
    if (size <= 1)
        return *this->playerSpawns.begin();

    //size-1
    return this->playerSpawns.at(urand(0, size-1));
};

//Changement du statut de la ffa
void FreeForAll::setStateFFA(FreeForAllState state)
{
    if (state == FREE_FOR_ALL_STATE_ENABLED)
        spawnEntity();
    else
        spawnEntity(true);
}

//Spawn des entity
void FreeForAll::spawnEntity(bool remove)
{
    if (remove)
        TC_LOG_INFO("server.loading", "[FreeForAll] Entity remove init...");
    else
        TC_LOG_INFO("server.loading", "[FreeForAll] Entity add init...");

    this->spawnGameObjectEntity(remove);
    this->spawnCreatureEntity(remove);
}

void FreeForAll::spawnGameObjectEntity(bool remove)
{
    for (std::vector<int32>::iterator itr = this->gameobjectGuidList.begin(); itr != this->gameobjectGuidList.end(); ++itr)
    {
        int32 guid = *itr;

        //Suppresion 
        //On inverse les guid
        if (remove)
        {
            if (*itr < 0)
                guid = abs(guid);
            else
                guid = -guid;
        }

        // Guid Positif
        // Ajout sur la carte
        if (guid > 0)
        {
            if (GameObjectData const* data = sObjectMgr->GetGOData(guid))
            {
                TC_LOG_INFO("server.loading", "[FreeForAll] Add gameobject %u...", guid);
                sObjectMgr->AddGameobjectToGrid(guid, data);

                // Spawn if necessary (loaded grids only)
                // this base map checked as non-instanced and then only existed
                Map* map = const_cast<Map*>(sMapMgr->CreateBaseMap(data->mapid));

                // We use current coords to unspawn, not spawn coords since creature can have changed grid
                if (!map->Instanceable() && map->IsGridLoaded(data->posX, data->posY))
                {
                    GameObject* pGameobject = new GameObject;
                    //sLog->outDebug("Spawning gameobject %u", *itr);
                    if (!pGameobject->LoadFromDB(guid, map))
                        delete pGameobject;
                    else
                    {
                        if (pGameobject->isSpawnedByDefault())
                            map->AddToMap(pGameobject);
                    }
                }
            }
        }
        else
        {
            guid = abs(guid);
            TC_LOG_INFO("server.loading", "[FreeForAll] Remove gameobject %u...", guid);

            if (GameObjectData const* data = sObjectMgr->GetGOData(guid))
            {
                sObjectMgr->RemoveGameobjectFromGrid(guid, data);

                if (GameObject* pGameobject = ObjectAccessor::GetObjectInWorld(MAKE_NEW_GUID(guid, data->id, HIGHGUID_GAMEOBJECT), (GameObject*)NULL))
                    pGameobject->AddObjectToRemoveList();
            }
        }
    }
}

void FreeForAll::spawnCreatureEntity(bool remove)
{
    for (std::vector<int32>::iterator itr = this->creatureGuidList.begin(); itr != this->creatureGuidList.end(); ++itr)
    {
        int32 guid = *itr;

        //Suppresion 
        //On inverse les guid
        if (remove)
        {
            if (*itr < 0)
                guid = abs(guid);
            else
                guid = -guid;
        }

        // Guid Positif
        // Ajout sur la carte
        if (guid > 0)
        {
            if (CreatureData const* data = sObjectMgr->GetCreatureData(guid))
            {
                TC_LOG_INFO("server.loading", "[FreeForAll] Add creature  %u...", guid);
                sObjectMgr->AddCreatureToGrid(guid, data);

                // Spawn if necessary (loaded grids only)
                Map* map = const_cast<Map*>(sMapMgr->CreateBaseMap(data->mapid));

                // We use spawn coords to spawn
                if (!map->Instanceable() && map->IsGridLoaded(data->posX, data->posY))
                {
                    Creature* pCreature = new Creature;

                    if (!pCreature->LoadFromDB(guid, map))
                        delete pCreature;
                    else
                        map->AddToMap(pCreature);
                }
            }
        }
        // Guid Négatif
        // On supprime de la carte
        else
        {
            guid = abs(guid);
            TC_LOG_INFO("server.loading", "[FreeForAll] Remove creature  %u...", guid);

            if (CreatureData const* data = sObjectMgr->GetCreatureData(guid))
            {
                sObjectMgr->RemoveCreatureFromGrid(guid, data);

                if (Creature* pCreature = ObjectAccessor::GetObjectInWorld(MAKE_NEW_GUID(guid, data->id, HIGHGUID_UNIT), (Creature*)NULL))
                    pCreature->AddObjectToRemoveList();
            }
        }
    }
}

bool FreeForAll::isFreeForAllArea(uint32 areaId)
{
    return std::find(this->areaIdList.begin(), this->areaIdList.end(), areaId) != this->areaIdList.end();
}

uint32 FreeForAll::getPlayerSpawnCount()
{
    uint32 count = this->playerSpawns.size();

    if (count == 0)
        TC_LOG_ERROR("server.loading", "[FFA] aucune téléportation pour la FFA %u", this->getId());

    return count;
}