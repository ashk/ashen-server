/*
* Ashen Core
*/

#ifndef NO_MANS_LAND_H_
#define NO_MANS_LAND_H_

#include <ace/Singleton.h>

class NoMansLand
{

private:
    std::vector<uint32> allowedArea; //Tableau dynamique

public:
    NoMansLand(); //Constructeur
    ~NoMansLand(); //Deconstruceur

    //Chargement du NoMansLand
    bool LoadNoMansLand();

    /*
        V�rifie si la zone est restreinte
        et t�l�porte le joueur a la zone voulue
    */
    bool isAllowed(uint32 areaId);

};

#define sNoMansLand ACE_Singleton<NoMansLand, ACE_Null_Mutex>::instance()

#endif // NO_MANS_LAND_H_
