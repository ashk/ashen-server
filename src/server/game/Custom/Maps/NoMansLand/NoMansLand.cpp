/*
* Ashen Core
*/

#include "NoMansLand.h"

NoMansLand::NoMansLand(){};
NoMansLand::~NoMansLand(){};

bool NoMansLand::LoadNoMansLand()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_NOMANSLAND_ALLOWED_AREA);
    PreparedQueryResult result = WorldDatabase.Query(stmt);

    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 Allowed Area. DB table `area_allowed` is empty.");
        return false;
    }

    if (result)
    {
        do
        {
            Field* fields = result->Fetch();
            uint32 areaId = fields[1].GetUInt32();
           
            allowedArea.push_back(areaId);

        } while (result->NextRow());
    }

    TC_LOG_INFO("server.loading", ">> Loaded %u allowed area %u ms", result->GetRowCount(), GetMSTimeDiffToNow(oldMSTime));
    return true;
};

bool NoMansLand::isAllowed(uint32 areaId)
{
    //L'area est autoris�
    return std::find(allowedArea.begin(), allowedArea.end(), areaId) != allowedArea.end();
};
