/*
* Ashen Core
*/


#include "CustomEvent.h"
#include "CustomEventMgr.h"
#include "Player.h"
#include "GameEventMgr.h"

CustomEvent::CustomEvent(uint32 id)
{
    this->id = id;
};

CustomEvent::~CustomEvent(){};

void CustomEvent::clearAllCheckPoint()
{
    this->checkPoint.clear();
};

void CustomEvent::clearCheckPoint(uint32 guid)
{
    if (this->checkPoint.find(guid) != this->checkPoint.end()) //L'entry existe
    {
        this->checkPoint.erase(guid);
    }
};

void CustomEvent::setCheckPoint(uint32 guid, CustomEventCheckPoint checkPoint)
{
    //Vérification checkPoint
    if (!MapManager::IsValidMapCoord(checkPoint.mapId, checkPoint.x, checkPoint.y, checkPoint.z, checkPoint.o))
    {
        TC_LOG_ERROR("server.loading", ">>[CustomEvent] Invalid  CheckPoint value for CustomEvent %u", this->id);
        return;
    }

    if (this->checkPoint.find(guid) != this->checkPoint.end()) //L'entry existe
        this->checkPoint.at(guid) = checkPoint;
    else
        this->checkPoint.insert(std::pair<uint32, CustomEventCheckPoint>(guid, checkPoint));
}

void CustomEvent::teleportToCheckPoint(uint32 guid)
{
    Player* player = sObjectMgr->GetPlayerByLowGUID(guid);
    if (player)
    {
        CustomEventCheckPoint checkPoint = defaultCheckPoint;
        
        if (player->IsGameMaster() || this->enabled) //Event Actif ou MJ
        {
            if (this->checkPoint.find(guid) != this->checkPoint.end()) //L'entry existe
                checkPoint = this->checkPoint.at(guid);
            else //L'entry n'existe pas on insert le checkPoint par default
                this->setCheckPoint(guid, checkPoint);

            player->TeleportTo(checkPoint.mapId, checkPoint.x, checkPoint.y, checkPoint.z, checkPoint.o);
        }
    }
};

bool CustomEvent::isAllowedArea(uint32 areaId)
{
    return std::find(this->allowedAreaList.begin(), this->allowedAreaList.end(), id) != this->allowedAreaList.end();
};

void CustomEvent::setState(bool enabled)
{
    this->enabled = enabled;
    
    if (enabled)
    {
        sCustomEventMgr->addCustomEventEnabled(this->id);
        sGameEventMgr->StartEvent(this->gameEventId, true);
        this->clearAllCheckPoint();
    }
    else
    {
        sCustomEventMgr->removeCustomEventEnabled(this->id);
        sGameEventMgr->StopEvent(this->gameEventId, true);  
    }

    this->clearAllCheckPoint();
};