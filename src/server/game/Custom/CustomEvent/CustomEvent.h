/*
* Ashen Core
*/

#ifndef CUSTOMEVENT_H_
#define CUSTOMEVENT_H_


struct CustomEventCheckPoint
{
    float x;
    float y;
    float z;
    uint32 mapId;
    float o;
};

class CustomEvent
{

public:
    CustomEvent(uint32 id); //Constructeur
    ~CustomEvent(); //Deconstruceur

    void clearAllCheckPoint(); // Nettoyage de tout les checks points
    void clearCheckPoint(uint32 guid); // Nettoyage du check point d'un joueur en particulier
    void teleportToCheckPoint(uint32 guid); // Teleporte un joueur au checkpoint sauvegarder
    bool isAllowedArea(uint32 areaId); //Verifie si c'est une area autorise
    void addAreaId(uint32 areaId){ this->allowedAreaList.push_back(areaId); };


    // Set
    void setAllowedAreaList(std::vector<uint32> allowedAreaList) { this->allowedAreaList = allowedAreaList; }; //Definis les AllowedArea
    void setState(bool enabled); // Active d�active l'event
    void setDefaultCheckPoint(CustomEventCheckPoint defaultCheckPoint) { this->defaultCheckPoint = defaultCheckPoint; }; //Met le checkpoint par defaut
    void setGameEventId(uint32 id) { this->gameEventId = id; };
    void setName(std::string name) { this->name = name; };
    void setCheckPoint(uint32 guid, CustomEventCheckPoint checkPoint);

    //Get
    bool getState() { return this->enabled; };
    uint32 getId() { return this->id; };
    std::string getName() { return this->name; };

private:
    uint32 gameEventId;
    uint32 id;
    std::string name;
    bool enabled = false;

    std::map<uint32, CustomEventCheckPoint> checkPoint;
    std::vector<uint32> allowedAreaList;

    CustomEventCheckPoint defaultCheckPoint;
};


#define sCustomEventMgr ACE_Singleton<CustomEventMgr, ACE_Null_Mutex>::instance()
#endif // CUSTOMEVENT_H_
