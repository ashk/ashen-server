/*
* Ashen Core
*/

#ifndef CUSTOMEVENTMGR_H_
#define CUSTOMEVENTMGR_H_

#include <ace/Singleton.h>
#include "CustomEvent.h"


class CustomEventMgr
{

public:
    CustomEventMgr(); //Constructeur
    ~CustomEventMgr(); //Deconstruceur

    bool loadCustomEvent();
   
    CustomEvent* getCustomEvent(uint32 id); // Retourne l'event
    bool isCustomEvent(uint32 id);
    std::map<uint32, CustomEvent*> getCustomEventList() { return this->customEventList; }; // Retourne la liste des customs event
    bool isCustomEventArea(uint32 areaId); //Verifie que le joueur est dans une area d'un Custom Event ACTIF
    void removeCustomEventEnabled(uint32 id);
    void addCustomEventEnabled(uint32 id);

private:
    std::map<uint32, CustomEvent*> customEventList;
    std::vector<uint32> customEventEnabled;
};


#define sCustomEventMgr ACE_Singleton<CustomEventMgr, ACE_Null_Mutex>::instance()
#endif // CUSTOMEVENTMGR_H_
