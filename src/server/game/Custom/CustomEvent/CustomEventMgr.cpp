/*
* Ashen Core
*/


#include "CustomEventMgr.h"
#include "CustomEvent.h"
#include "GameEventMgr.h"
#include "DBCStores.h"

CustomEventMgr::CustomEventMgr(){};
CustomEventMgr::~CustomEventMgr(){};

//Loader
bool CustomEventMgr::loadCustomEvent()
{
    uint32 oldMSTime = getMSTime();
    this->customEventList.clear();

    QueryResult result = WorldDatabase.Query("SELECT entry, eventEntry, areaId, name, default_map, default_x, default_y, default_z, default_orientation FROM custom_event_template");

    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 Custom Event. DB table `custom_event_template` is empty.");
        return false;
    }

    uint32 count = 0;
    do
    {
        Field *fields = result->Fetch();

        uint32 id = fields[0].GetUInt32();
        uint32 eventEntry = fields[1].GetUInt32();
        std::string areaIdStr = fields[2].GetString();
        std::string name = fields[3].GetString();
        uint32 default_map = fields[4].GetUInt32();
        float default_x = fields[5].GetFloat();
        float default_y = fields[6].GetFloat();
        float default_z = fields[7].GetFloat();
        float default_o = fields[8].GetFloat();


        CustomEvent* oCustomEvent = new CustomEvent(id);

        //Vérification checkPoint
        if (!MapManager::IsValidMapCoord(default_map, default_x, default_y, default_z, default_o))
        {
            TC_LOG_ERROR("server.loading", ">>[CustomEvent] Invalid CustomEvent Default CheckPoint Coord %u - Skipped", id);
            continue;
        }

        //Default CheckPoint
        CustomEventCheckPoint checkPoint;
        checkPoint.mapId = default_map;
        checkPoint.x = default_x;
        checkPoint.y = default_y;
        checkPoint.z = default_z;
        checkPoint.o = default_o;

        oCustomEvent->setDefaultCheckPoint(checkPoint);

        //AreaID
        char* areaIdArgs = strtok((char*)areaIdStr.c_str(), ";");

        if (areaIdArgs == NULL || atoi(areaIdArgs) == 0)
        {
            TC_LOG_ERROR("server.loading", ">>  No Area for Custom Event %u Skipped", id);
            continue;
        }

        do
        {
            uint32 areaId = atoi(areaIdArgs); //Conversion en nombre

            if (GetAreaEntryByAreaID(areaId)) //L'area existe
            {
                TC_LOG_ERROR("server.loading", ">>  Area %u added for Custom Event %u", areaId, id);
                oCustomEvent->addAreaId(areaId);
            }
            else
                TC_LOG_ERROR("server.loading", ">>  Area %u is invalid area for Custom Event %u", areaId, id);

            areaIdArgs = strtok(NULL, ";");
        } while (areaIdArgs != NULL);

        //Game Event
        GameEventMgr::GameEventDataMap const& events = sGameEventMgr->GetEventMap();
        GameEventData const& eventData = events[eventEntry];
        if (!eventData.isValid())
        {
            TC_LOG_ERROR("server.loading", ">>[CustomEvent] Invalid Game Event %u (CustomEvent ID %u) - Skipped", eventEntry, id);
            continue;
        }

        oCustomEvent->setGameEventId(eventEntry);
        oCustomEvent->setName(name);

        this->customEventList.insert(std::pair<uint32, CustomEvent*>(id, oCustomEvent));   
        count++;

    } while (result->NextRow());


    TC_LOG_INFO("server.loading", ">> Loaded %u CustomEvent %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    return true;


    return true;
};


CustomEvent* CustomEventMgr::getCustomEvent(uint32 id)
{
    if (this->customEventList.find(id) != this->customEventList.end())
    {
        return this->customEventList.at(id);
    }
    else
        return NULL;
};

bool CustomEventMgr::isCustomEventArea(uint32 areaId)
{
    for (std::vector<uint32>::iterator itr = this->customEventEnabled.begin(); itr != this->customEventEnabled.end(); ++itr)
    {
        if (CustomEvent* customEvent = this->getCustomEvent(*itr))
        {
            if (!customEvent->isAllowedArea(areaId))
                return false;
        }
    }
    return true;
};

bool CustomEventMgr::isCustomEvent(uint32 id)
{
    return this->customEventList.find(id) != this->customEventList.end();
};


void CustomEventMgr::addCustomEventEnabled(uint32 id)
{
    std::vector<uint32>::iterator itr = std::find(this->customEventEnabled.begin(), this->customEventEnabled.end(), id);

    if (itr == this->customEventEnabled.end())
        this->customEventEnabled.push_back(id);
}

void CustomEventMgr::removeCustomEventEnabled(uint32 id)
{
    std::vector<uint32>::iterator itr = std::find(this->customEventEnabled.begin(), this->customEventEnabled.end(), id);
    
    if (itr != this->customEventEnabled.end())
        this->customEventEnabled.erase(itr);
}