#include "MonitoringDatabase.h"

void MonitoringDatabaseConnection::DoPrepareStatements()
{
    if (!m_reconnecting)
        m_stmts.resize(MAX_MONITORINGDATABASE_STATEMENTS);

    PrepareStatement(MONITORING_UPD_LAST_UPDATE, "UPDATE last_update SET timestamp = UNIX_TIMESTAMP()", CONNECTION_ASYNC);
    PrepareStatement(MONITORING_INS_STATS, "INSERT INTO stats (TimeStamp, OnlinePlayers, OnlineGameMasters, Uptime, UpdateDiff, Upload, Download) VALUES (UNIX_TIMESTAMP(), ?, ?, ?, ?, ?, ?)", CONNECTION_ASYNC);
    PrepareStatement(MONITORING_INS_CHARACTER_STATS, "SELECT 1", CONNECTION_ASYNC);
}
