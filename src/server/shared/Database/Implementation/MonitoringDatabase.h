#ifndef _MONITORINGDATABASE_H
#define _MONITORINGDATABASE_H

#include "DatabaseWorkerPool.h"
#include "MySQLConnection.h"

class MonitoringDatabaseConnection : public MySQLConnection
{
    public:
        //- Constructors for sync and async connections
        MonitoringDatabaseConnection(MySQLConnectionInfo& connInfo) : MySQLConnection(connInfo) {}
        MonitoringDatabaseConnection(ACE_Activation_Queue* q, MySQLConnectionInfo& connInfo) : MySQLConnection(q, connInfo) {}

        //- Loads database type specific prepared statements
        void DoPrepareStatements();
};

typedef DatabaseWorkerPool<MonitoringDatabaseConnection> MonitoringDatabaseWorkerPool;

enum MonitoringDatabaseStatements
{
    /*  Naming standard for defines:
        {DB}_{SEL/INS/UPD/DEL/REP}_{Summary of data changed}
        When updating more than one field, consider looking at the calling function
        name for a suiting suffix.
    */

    MONITORING_UPD_LAST_UPDATE,
    MONITORING_INS_STATS,
    MONITORING_INS_CHARACTER_STATS,

    MAX_MONITORINGDATABASE_STATEMENTS
};

#endif