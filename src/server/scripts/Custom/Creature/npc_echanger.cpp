#include "ScriptPCH.h"
 
class npc_echanger : public CreatureScript
{
public:
    npc_echanger() : CreatureScript("npc_echanger") { }
 
    bool OnGossipHello(Player *player, Creature *creature)
    {
        player->ADD_GOSSIP_ITEM(0, "|cff4169E1|tConvertir 100 points d'Arène en 500 points d'Honneurs", GOSSIP_SENDER_MAIN, 1);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player *player, Creature *creature, uint32 sender, uint32 uiAction)
    {
        if (sender == GOSSIP_SENDER_MAIN)
        {
            switch(uiAction)
            {
                /* GossipMenu */
                case 1:    
                    if (player->GetArenaPoints() >= 100)
                    {
                        player->CLOSE_GOSSIP_MENU();
                        player->ModifyArenaPoints(-100);
                        player->ModifyHonorPoints(500);
                        player->GetSession()->SendAreaTriggerMessage("Vous venez de convertir 100 points d'arènes en 500 points d'honneurs !");
                    }
                    else
                    {
                        player->CLOSE_GOSSIP_MENU();
                        player->GetSession()->SendAreaTriggerMessage("Vous n'avez pas assez de Points d'Arène pour cela !");
                    }
                    break;
            }
        }

        return true;
    }
};

void AddSC_npc_echanger()
{
    new npc_echanger();
}