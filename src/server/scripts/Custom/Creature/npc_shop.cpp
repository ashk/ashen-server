// Daone - Socra CopyRight - Boutique C++ (Personnel)

#include "ScriptPCH.h"

using namespace std;
class npc_shop : public CreatureScript
{
public:
    npc_shop() : CreatureScript("npc_shop") { }

    bool OnGossipHello(Player *player, Creature *creature)
    {
        player->ADD_GOSSIP_ITEM(0, "|cff4169E1|TInterface\\icons\\Inv_feather_12:30:30:-24:0|t Boutique En Jeu", GOSSIP_SENDER_MAIN, 1);
        player->ADD_GOSSIP_ITEM(0, "|cff4169E1|TInterface\\icons\\Inv_feather_13:30:30:-24:0|t Gestion de Compte", GOSSIP_SENDER_MAIN, 2);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());         
        return true;
    }    

    bool OnGossipSelect(Player *player, Creature *creature, uint32 sender, uint32 uiAction)
    {
        QueryResult select = LoginDatabase.PQuery("SELECT id,vp,dp FROM account_data WHERE id = '%u'", player->GetSession()->GetAccountId());
        if(!select)
        {
            player->GetSession()->SendAreaTriggerMessage("Erreur Interne");
            return false;
        }
        Field* fields = select->Fetch();
        uint32 id = fields[0].GetUInt32();
        uint32 vp = fields[1].GetUInt32();
        uint32 dp = fields[2].GetUInt32();
        
        player->PlayerTalkClass->ClearMenus();
        
        if (sender == GOSSIP_SENDER_MAIN)
        {
            switch(uiAction)
            {
                /* GossipMenu */
                case 1:    
                {
                    player->ADD_GOSSIP_ITEM(0, "|cff4169E1|TInterface\\icons\\INV_Inscription_ArmorScroll01:30:30:-24:0|t Les Tokens Boutique", GOSSIP_SENDER_MAIN, 10);
                    player->ADD_GOSSIP_ITEM(0, "|cff4169E1|TInterface\\icons\\INV_Misc_Token_ArgentDawn:30:30:-24:0|t Les Tokens en Jeu", GOSSIP_SENDER_MAIN, 11);
                    player->ADD_GOSSIP_ITEM(0, "|cff4169E1|TInterface\\icons\\INV_Inscription_Papyrus:30:30:-24:0|t Autres modifications", GOSSIP_SENDER_MAIN, 15);
                    player->ADD_GOSSIP_ITEM(0, "|cffff0000|TInterface\\icons\\INV_Pet_CelestialDragon:30:30:-24:0|t Monture Inédites [A VENIR]", GOSSIP_SENDER_MAIN, 12);
                    player->ADD_GOSSIP_ITEM(0, "|cffff0000|TInterface\\icons\\INV_Pet_PinkMurlocEgg:30:30:-24:0|t Compagnons Inédits [A VENIR]", GOSSIP_SENDER_MAIN, 13);
                    player->ADD_GOSSIP_ITEM(0, "|cffff0000|TInterface\\icons\\ABILITY_SHOOTWAND:30:30:-24:0|t Auras Inédites [A VENIR]", GOSSIP_SENDER_MAIN, 14);
                    player->ADD_GOSSIP_ITEM(4, "Retour", GOSSIP_SENDER_MAIN, 500);
                    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());  
                    break;
                }
                
                case 2:    
                {
                    player->ADD_GOSSIP_ITEM_EXTENDED(5, "Mes Derniers Achats", GOSSIP_SENDER_MAIN, 300, "", 0, false);
                    player->ADD_GOSSIP_ITEM( 4, "Afficher Mes Points", GOSSIP_SENDER_MAIN, 400);
                    player->ADD_GOSSIP_ITEM(4, "...Retour", GOSSIP_SENDER_MAIN, 500);
                    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());  
                    break;
                }
                
                /* Sous Menu */
                case 10:
                {
                    player->ADD_GOSSIP_ITEM(0, "1 Token VIP |cff0000ff[100 AP]", GOSSIP_SENDER_MAIN, 1000);
                    player->ADD_GOSSIP_ITEM(0, "2 Token VIP |cff0000ff[175 AP]", GOSSIP_SENDER_MAIN, 1001);
                    player->ADD_GOSSIP_ITEM(0, "4 Token VIP |cff0000ff[300 AP]", GOSSIP_SENDER_MAIN, 1002);
                    player->ADD_GOSSIP_ITEM(4, "Retour", GOSSIP_SENDER_MAIN, 1);
                    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                    break;
                }
                
                case 11:
                {
                    player->ADD_GOSSIP_ITEM(0, "100 Tokens Fun |cff0000ff[50 AP]", GOSSIP_SENDER_MAIN, 1100);
                    player->ADD_GOSSIP_ITEM(0, "1000 Tokens Fun |cff0000ff[400 AP]", GOSSIP_SENDER_MAIN, 1101);
                    player->ADD_GOSSIP_ITEM(4, "Retour", GOSSIP_SENDER_MAIN, 1);
                    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                    break;
                }
                
                case 12:
                {
                    player->ADD_GOSSIP_ITEM(0, "Monture 1", GOSSIP_SENDER_MAIN, 1200);
                    player->ADD_GOSSIP_ITEM(0, "Monture 2", GOSSIP_SENDER_MAIN, 1201);
                    player->ADD_GOSSIP_ITEM(0, "Monture 3", GOSSIP_SENDER_MAIN, 1202);
                    player->ADD_GOSSIP_ITEM(0, "Monture 4", GOSSIP_SENDER_MAIN, 1203);
                    player->ADD_GOSSIP_ITEM(4, "Retour", GOSSIP_SENDER_MAIN, 1);
                    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                    break;
                }
                
                case 13: 
                {
                    player->ADD_GOSSIP_ITEM(0, "Compagnons 1", GOSSIP_SENDER_MAIN, 1300);
                    player->ADD_GOSSIP_ITEM(0, "Compagnons 2", GOSSIP_SENDER_MAIN, 1301);
                    player->ADD_GOSSIP_ITEM(0, "Compagnons 3", GOSSIP_SENDER_MAIN, 1302);
                    player->ADD_GOSSIP_ITEM(0, "Compagnons 4", GOSSIP_SENDER_MAIN, 1303);
                    player->ADD_GOSSIP_ITEM(4, "Retour", GOSSIP_SENDER_MAIN, 1);
                    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                    break;
                }
                
                case 14:
                {
                    player->ADD_GOSSIP_ITEM(0, "Auras 1", GOSSIP_SENDER_MAIN, 1400);
                    player->ADD_GOSSIP_ITEM(0, "Auras 2", GOSSIP_SENDER_MAIN, 1401);
                    player->ADD_GOSSIP_ITEM(0, "Auras 3", GOSSIP_SENDER_MAIN, 1402);
                    player->ADD_GOSSIP_ITEM(0, "Auras 4", GOSSIP_SENDER_MAIN, 1403);
                    player->ADD_GOSSIP_ITEM(4, "Retour", GOSSIP_SENDER_MAIN, 1);
                    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                    break;
                }
                
                case 15:
                {
                    player->ADD_GOSSIP_ITEM(0, "Changer de Nom |cff0000ff[250 AP]", GOSSIP_SENDER_MAIN, 1500);
                    player->ADD_GOSSIP_ITEM(0, "Changer de Race |cff0000ff[300 AP]", GOSSIP_SENDER_MAIN, 1501);
                    player->ADD_GOSSIP_ITEM(0, "Changer de Faction |cff0000ff[350 AP]", GOSSIP_SENDER_MAIN, 1502);
                    player->ADD_GOSSIP_ITEM(4, "Retour", GOSSIP_SENDER_MAIN, 1);
                    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                    break;
                }
                
                /* Prix et Achate */
                    /* Equipement Boutique */
                case 1000: // Full Equipement Vip
                {
                    if (vp < 100)
                    {
                        player->GetSession()->SendAreaTriggerMessage("Vous n'avez pas assez d'Ashen Points pour cet achat !");
                        player->CLOSE_GOSSIP_MENU();
                    }
                    else        
                    {
                        LoginDatabase.PExecute("UPDATE account_data SET vp = '%u' -100 WHERE id = '%u'", vp, player->GetSession()->GetAccountId()); 
                        player->AddItem(80004, 1);
                        LoginDatabase.PExecute("INSERT INTO boutique_log (account_id,  nom, produit, produit_nom, prix) VALUES ('%u', '%s', '1', '1 Token VIP', '1000')", player->GetSession()->GetAccountId(), player->GetName().c_str());
                        player->GetSession()->SendAreaTriggerMessage("Vous avez reçu 1 Token VIP");
                        player->SaveToDB();
                        player->CLOSE_GOSSIP_MENU();
                    }        
                    break;
                }
                
                case 1001: // Full Equipement Vip
                {
                    if (vp < 175)
                    {
                        player->GetSession()->SendAreaTriggerMessage("Vous n'avez pas assez d'Ashen Points pour cet achat !");
                        player->CLOSE_GOSSIP_MENU();
                    }
                    else        
                    {
                        LoginDatabase.PExecute("UPDATE account_data SET vp = '%u' -175 WHERE id = '%u'", vp, player->GetSession()->GetAccountId()); 
                        player->AddItem(80004, 2);
                        LoginDatabase.PExecute("INSERT INTO boutique_log (account_id,  nom, produit, produit_nom, prix) VALUES ('%u', '%s', '1', '2 Token VIP', '175')", player->GetSession()->GetAccountId(), player->GetName().c_str());
                        player->GetSession()->SendAreaTriggerMessage("Vous avez reçu 2 Token VIP");
                        player->SaveToDB();
                        player->CLOSE_GOSSIP_MENU();
                    }        
                    break;
                }
                
                case 1002: // Full Equipement Vip
                {
                    if (vp < 300)
                    {
                        player->GetSession()->SendAreaTriggerMessage("Vous n'avez pas assez d'Ashen Points pour cet achat !");
                        player->CLOSE_GOSSIP_MENU();
                    }
                    else        
                    {
                        LoginDatabase.PExecute("UPDATE account_data SET vp = '%u' -300 WHERE id = '%u'", vp, player->GetSession()->GetAccountId()); 
                        player->AddItem(80004, 4);
                        LoginDatabase.PExecute("INSERT INTO boutique_log (account_id,  nom, produit, produit_nom, prix) VALUES ('%u', '%s', '1', '4 Token VIP', '300')", player->GetSession()->GetAccountId(), player->GetName().c_str());
                        player->GetSession()->SendAreaTriggerMessage("Vous avez reçu 4 Token VIP");
                        player->SaveToDB();
                        player->CLOSE_GOSSIP_MENU();
                    }        
                    break;
                }
                    /* Tokens */
                case 1100: // 100 Tokens Fun
                {
                    if (vp < 50)
                    {
                        player->GetSession()->SendAreaTriggerMessage("Vous n'avez pas assez d'Ashen Points pour cet achat !");
                        player->CLOSE_GOSSIP_MENU();
                    }
                    else        
                    {
                        LoginDatabase.PExecute("UPDATE account_data SET vp = '%u' -50 WHERE id = '%u'", vp, player->GetSession()->GetAccountId()); 
                        player->AddItem(80003, 100);
                        LoginDatabase.PExecute("INSERT INTO boutique_log (account_id,  nom, produit, produit_nom, prix) VALUES ('%u', '%s', '1', '100 Tokens Funs', '50')", player->GetSession()->GetAccountId(), player->GetName().c_str());
                        player->GetSession()->SendAreaTriggerMessage("Vous avez reçu 100 Tokens Fun");
                        player->SaveToDB();
                        player->CLOSE_GOSSIP_MENU();
                    }        
                    break;
                }
                
                case 1101:                // 1000 Tokens Fun
                {
                    if (vp < 400)
                    {
                        player->GetSession()->SendAreaTriggerMessage("Vous n'avez pas assez d'Ashen Points pour cet achat !");
                        player->CLOSE_GOSSIP_MENU();
                    }
                    else        
                    {
                        LoginDatabase.PExecute("UPDATE account_data SET vp = '%u' -400 WHERE id = '%u'", vp, player->GetSession()->GetAccountId()); 
                        player->AddItem(80003, 1000);
                        LoginDatabase.PExecute("INSERT INTO boutique_log (account_id,  nom, produit, produit_nom, prix) VALUES ('%u', '%s', '1', '1000 Tokens Funs', '400')", player->GetSession()->GetAccountId(), player->GetName().c_str());
                        player->GetSession()->SendAreaTriggerMessage("Vous avez reçu 1000 Tokens Fun");
                        player->SaveToDB();
                        player->CLOSE_GOSSIP_MENU();
                    }        
                    break;
                }
                    /* Montures */
                    
                    /* Compagnons */
                    
                    /* Auras */
                    
                    /* Autres Modifications */
                    
                case 1500: // Change Nom
                {
                    if (vp < 100)
                    {
                        player->GetSession()->SendAreaTriggerMessage("Vous n'avez pas assez d'Ashen Points pour cet achat !");
                        player->CLOSE_GOSSIP_MENU();
                    }
                    else        
                    {
                        LoginDatabase.PExecute("UPDATE account_data SET vp = '%u' -100 WHERE id = '%u'", vp, player->GetSession()->GetAccountId()); 
                        player->SetAtLoginFlag(AT_LOGIN_RENAME);
                        LoginDatabase.PExecute("INSERT INTO boutique_log (account_id,  nom, produit, produit_nom, prix) VALUES ('%u', '%s', '1', 'Changement de Nom', '100')", player->GetSession()->GetAccountId(), player->GetName().c_str());
                        player->GetSession()->SendAreaTriggerMessage("Deconnectez vous pour changer votre nom de personnage !");
                        player->SaveToDB();
                        player->CLOSE_GOSSIP_MENU();
                    }        
                    break;
                }
                
                case 1501: // Change Race
                {
                    if (vp < 150)
                    {
                        player->GetSession()->SendAreaTriggerMessage("Vous n'avez pas assez d'Ashen Points pour cet achat !");
                        player->CLOSE_GOSSIP_MENU();
                    }
                    else        
                    {
                        LoginDatabase.PExecute("UPDATE account_data SET vp = '%u' -150 WHERE id = '%u'", vp, player->GetSession()->GetAccountId()); 
                        player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
                        LoginDatabase.PExecute("INSERT INTO boutique_log (account_id,  nom, produit, produit_nom, prix) VALUES ('%u', '%s', '1', 'Changement de Race', '150')", player->GetSession()->GetAccountId(), player->GetName().c_str());
                        player->GetSession()->SendAreaTriggerMessage("Deconnectez vous pour changer votre race !");
                        player->SaveToDB();
                        player->CLOSE_GOSSIP_MENU();
                    }        
                    break;
                }
                
                case 1502: // Change Faction
                {
                    if (vp < 200)
                    {
                        player->GetSession()->SendAreaTriggerMessage("Vous n'avez pas assez d'Ashen Points pour cet achat !");
                        player->CLOSE_GOSSIP_MENU();
                    }
                    else        
                    {
                        LoginDatabase.PExecute("UPDATE account_data SET vp = '%u' -200 WHERE id = '%u'", vp, player->GetSession()->GetAccountId()); 
                        player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
                        LoginDatabase.PExecute("INSERT INTO boutique_log (account_id,  nom, produit, produit_nom, prix) VALUES ('%u', '%s', '1', 'Changement de Faction', '200')", player->GetSession()->GetAccountId(), player->GetName().c_str());
                        player->GetSession()->SendAreaTriggerMessage("Deconnectez vous pour changer votre faction !");
                        player->SaveToDB();
                        player->CLOSE_GOSSIP_MENU();
                    }        
                    break;
                }
                /* Fonction Essentielles */
                case 400: // Nombre d'Ashen Points sur le compte
                {
                    player->GetSession()->SendAreaTriggerMessage("Vous disposez actuellement de %u Ashen Points", vp);   
                    break;    
                }
                
                case 500: // Retour
                {
                    OnGossipHello(player, creature);
                    break;
                }
                
                case 300: // Affichage des Derniers achats
                {
                    QueryResult result =LoginDatabase.PQuery("SELECT produit_nom, produit, prix FROM boutique_log WHERE account_id = '%u'  ORDER BY account_id DESC LIMIT 5", player->GetSession()->GetAccountId());
                    if (!result)
                        return false;

                    Field * fields = NULL;
                
                    do
                    {
                        fields = result->Fetch();
                        string produit_nom = fields[0].GetString();
                        uint32 produit = fields[1].GetUInt32();
                        uint32 prix = fields[2].GetUInt32();
                        ChatHandler(player->GetSession()).PSendSysMessage("Nom du Produit:  |cffFFFF00%s|r, Produit ID: %u ,  Prix du Produit: %u \n", produit_nom.c_str(),  produit, prix);
                    } while (result->NextRow());

                    player->PlayerTalkClass->SendCloseGossip();
                    break;
                }
            }
        }
        return false;
    }
};

void AddSC_npc_shop()
{
    new npc_shop();
}