#include "ScriptMgr.h"
#include "ArenaTeamMgr.h"
#include "Common.h"
#include "DisableMgr.h"
#include "BattlegroundMgr.h"
#include "Battleground.h"
#include "ArenaTeam.h"
#include "Language.h"

class npc_1v1arena : public CreatureScript
{
public:
    npc_1v1arena() : CreatureScript("npc_1v1arena") 
    {
    }


    bool JoinQueueArena(Player* player, Creature* me, bool isRated)
    {
        if(!player || !me)
            return false;

        if( 80  > player->getLevel())
            return false;

        uint64 guid = player->GetGUID();
        uint8 arenaslot = ArenaTeam::GetSlotByType(ARENA_TEAM_5v5);
        uint8 arenatype = ARENA_TYPE_5v5;
        uint32 arenaRating = 0;
        uint32 matchmakerRating = 0;

        // ignore if we already in BG or BG queue
        if (player->InBattleground())
            return false;

        //check existance
        Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_AA);
        if (!bg)
        {
            
            return false;
        }

        if (DisableMgr::IsDisabledFor(DISABLE_TYPE_BATTLEGROUND, BATTLEGROUND_AA, NULL))
        {
            ChatHandler(player->GetSession()).PSendSysMessage(LANG_ARENA_DISABLED);
            return false;
        }

        BattlegroundTypeId bgTypeId = bg->GetTypeID();
        BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeId, arenatype);
        PvPDifficultyEntry const* bracketEntry = GetBattlegroundBracketByLevel(bg->GetMapId(), player->getLevel());
        if (!bracketEntry)
            return false;

        GroupJoinBattlegroundResult err = ERR_GROUP_JOIN_BATTLEGROUND_FAIL;

        // check if already in queue
        if (player->GetBattlegroundQueueIndex(bgQueueTypeId) < PLAYER_MAX_BATTLEGROUND_QUEUES)
            //player is already in this queue
            return false;
        // check if has free queue slots
        if (!player->HasFreeBattlegroundQueueId())
            return false;

        uint32 ateamId = 0;

        if(isRated)
        {
            ateamId = player->GetArenaTeamId(arenaslot);
            ArenaTeam* at = sArenaTeamMgr->GetArenaTeamById(ateamId);
            if (!at)
            {
                player->GetSession()->SendNotInArenaTeamPacket(arenatype);
                return false;
            }

            // get the team rating for queueing
            arenaRating = at->GetRating();
            matchmakerRating = arenaRating;
            // the arenateam id must match for everyone in the group

            if (arenaRating <= 0)
                arenaRating = 1;
        }

        BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);
        bg->SetRated(isRated);

        GroupQueueInfo* ginfo = bgQueue.AddGroup(player, NULL, bgTypeId, bracketEntry, arenatype, isRated, false, arenaRating, matchmakerRating, ateamId);
        uint32 avgTime = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());
        uint32 queueSlot = player->AddBattlegroundQueueId(bgQueueTypeId);

        WorldPacket data;
        // send status packet (in queue)
        sBattlegroundMgr->BuildBattlegroundStatusPacket(&data, bg, queueSlot, STATUS_WAIT_QUEUE, avgTime, 0, arenatype, 0);
        player->GetSession()->SendPacket(&data);

        sBattlegroundMgr->ScheduleQueueUpdate(matchmakerRating, arenatype, bgQueueTypeId, bgTypeId, bracketEntry->GetBracketId());

        return true;
    }


    bool CreateArenateam(Player* player, Creature* me)
    {
        if(!player || !me)
            return false;

        uint8 slot = ArenaTeam::GetSlotByType(ARENA_TEAM_5v5);
        if (slot >= MAX_ARENA_SLOT)
            return false;

        // Check if player is already in an arena team
        if (player->GetArenaTeamId(slot))
        {
            player->GetSession()->SendArenaTeamCommandResult(ERR_ARENA_TEAM_CREATE_S, player->GetName(), "", ERR_ALREADY_IN_ARENA_TEAM);
            return false;
        }


        // Teamname = playername
        // if teamname exist, we have to choose another name (playername + number)
        int i = 1;
        std::stringstream teamName;
        teamName << player->GetName();
        do
        {
            if(sArenaTeamMgr->GetArenaTeamByName(teamName.str()) != NULL) // teamname exist, so choose another name
            {
                teamName.str(std::string());
                teamName << player->GetName() << i;
            }
            else
                break;
        } while (i < 100); // should never happen

        // Create arena team
        ArenaTeam* arenaTeam = new ArenaTeam();

        if (!arenaTeam->Create(player->GetGUID(), ARENA_TEAM_5v5, teamName.str(), 4283124816, 45, 4294242303, 5, 4294705149))
        {
            delete arenaTeam;
            return false;
        }

        // Register arena team
        sArenaTeamMgr->AddArenaTeam(arenaTeam);
        arenaTeam->AddMember(player->GetGUID());

        ChatHandler(player->GetSession()).SendSysMessage("Equipe d'Arene 1v1 Creer !");

        return true;
    }


    bool OnGossipHello(Player* player, Creature* me)
    {
        if(!player || !me)
            return true;

        if(sWorld->getBoolConfig(CONFIG_ARENA_1V1_ENABLE) == false)
        {
            ChatHandler(player->GetSession()).SendSysMessage("1v1 Desactiver !");
            return true;
        }

        if(player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_5v5))
                player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "|cff4169E1|TInterface\\icons\\Achievement_featsofstrength_gladiator_06:30:30:-24:0|tQuitter la file d'attente", GOSSIP_SENDER_MAIN, 3, "Vous etes sur ?", 0, false);
        else
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|cff4169E1|TInterface\\icons\\Achievement_arena_2v2_7:30:30:-24:0|tEntrer dans la file d'attente 1v1", GOSSIP_SENDER_MAIN, 2);

        if(player->GetArenaTeamId(ArenaTeam::GetSlotByType(ARENA_TEAM_5v5)) == NULL)
            player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "|cff4169E1|TInterface\\icons\\Achievement_bg_winwsg:30:30:-24:0|tCreer mon equipe 1v1   !", GOSSIP_SENDER_MAIN, 1, "Creer mon equipe 1v1 ?", 0, false);
        else
        {
            if(player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_5v5) == false)
            {
                
                player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "|cff4169E1|TInterface\\icons\\Inv_misc_coin_02:30:30:-24:0|tSuprimmer mon Equipe", GOSSIP_SENDER_MAIN, 5, "Vous etes sur ?", 0, false);
            }

            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|cff4169E1|TInterface\\icons\\Achievement_bg_ab_defendflags:30:30:-24:0|tVoir mes stats !", GOSSIP_SENDER_MAIN, 4);
        }

        
        player->SEND_GOSSIP_MENU(68, me->GetGUID());
        return true;
    }



    bool OnGossipSelect(Player* player, Creature* me, uint32 /*uiSender*/, uint32 uiAction)
    {
        if(!player || !me)
            return true;

        player->PlayerTalkClass->ClearMenus();

        switch (uiAction)
        {
        case 1: // Create new Arenateam
            {
                if(80 <= player->getLevel())
                {
                    if(player->GetMoney() >= 1 && CreateArenateam(player, me))
                        player->ModifyMoney(10);
                }
                else
                {
                    ChatHandler(player->GetSession()).PSendSysMessage("You need level  to create an 1v1 arenateam.");
                    player->CLOSE_GOSSIP_MENU();
                    return true;
                }
            }
            break;

        case 2: // Join Queue Arena (rated)
            {
                if  ( JoinQueueArena(player, me, true) == false)
                    ChatHandler(player->GetSession()).SendSysMessage("Erreur de Tag   !");
                
                player->CLOSE_GOSSIP_MENU();
                return true;
            }
            break;

        case 20: // Join Queue Arena (unrated)
            {
                if  ( JoinQueueArena(player, me, true) == false)
                    ChatHandler(player->GetSession()).SendSysMessage("Erreur de Tag  !");
                
                player->CLOSE_GOSSIP_MENU();
                return true;
            }
            break;

        case 3: // Leave Queue
            {
                WorldPacket Data;
                Data << (uint8)0x1 << (uint8)0x0 << (uint32)BATTLEGROUND_AA << (uint16)0x0 << (uint8)0x0;
                player->GetSession()->HandleBattleFieldPortOpcode(Data);
                player->CLOSE_GOSSIP_MENU();
                return true;
            }
            break;

        case 4: // get statistics
            {
                ArenaTeam* at = sArenaTeamMgr->GetArenaTeamById(player->GetArenaTeamId(ArenaTeam::GetSlotByType(ARENA_TEAM_5v5)));
                if(at)
                {
                    std::stringstream s;
                    s << "Cote: " << at->GetStats().Rating;
                    s << "\nRang:  " << at->GetStats().Rank;
                    s << "\nMatchs en Saison: " << at->GetStats().SeasonGames;
                    s << "\nVictoire de Saison: " << at->GetStats().SeasonWins;
                    s << "\nMatch en Week End: " << at->GetStats().WeekGames;
                    s << "\nMatch Week End Victoires: " << at->GetStats().WeekWins;

                    ChatHandler(player->GetSession()).PSendSysMessage(s.str().c_str());
                }
            }
            break;


        case 5: // Disband arenateam
            {
                WorldPacket Data;
                Data << (uint32)player->GetArenaTeamId(ArenaTeam::GetSlotByType(ARENA_TEAM_5v5));
                player->GetSession()->HandleArenaTeamLeaveOpcode(Data);
                ChatHandler(player->GetSession()).SendSysMessage("Ta team a ete suprimmer.");
                player->CLOSE_GOSSIP_MENU();
                return true;
            }
            break;

    

        }

        OnGossipHello(player, me);
        return true;
    }
};


void AddSC_npc_1v1arena()
{
    new npc_1v1arena();
}