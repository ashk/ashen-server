/*
 * Daone Copyright, NO DELETE OR ATTACK FOR YOU SERVER / Devellopers C++/Java/Python/Perl ect...
  */

#include "ScriptPCH.h"
#include <cstring>
#include <string.h>
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "ArenaTeam.h"
#include "ArenaTeamMgr.h"
#include "World.h"

enum ArenaRankActionIds 
{
    ARENA_2V2_LADDER = GOSSIP_ACTION_INFO_DEF + 1,
    ARENA_3V3_LADDER = GOSSIP_ACTION_INFO_DEF + 2,
    ARENA_5V5_LADDER = GOSSIP_ACTION_INFO_DEF + 3,
    TOP_VH = GOSSIP_ACTION_INFO_DEF + 5,
    ARENA_GOODBYE = GOSSIP_ACTION_INFO_DEF + 4,
    ARENA_NOOP = 1,
    ARENA_START_TEAM_LOOKUP = GOSSIP_ACTION_INFO_DEF + 5,
    
};

enum ArenaRankOptions 
{
    ARENA_MAX_RESULTS = 30,
};

enum ArenaGossipText 
{
    ARENA_GOSSIP_HELLO = 11201,
    ARENA_GOSSIP_NOTEAMS = 11202,
    ARENA_GOSSIP_TEAMS = 11203,
    ARENA_GOSSIP_TEAM_LOOKUP = 11204,
    
};

class npc_pvp_rating : public CreatureScript
{
    private:
        uint32 optionToTeamType(uint32 option) {
            uint32 teamType;
            switch(option) {
                case ARENA_2V2_LADDER: teamType = 2; break;
                case ARENA_3V3_LADDER: teamType = 3; break;
                case ARENA_5V5_LADDER: teamType = 5; break;
            }
            return teamType;
        }
        uint32 teamTypeToOption(uint32 teamType) {
            uint32 option;
            switch(teamType) {
                case 2: option = ARENA_2V2_LADDER; break;
                case 3: option = ARENA_3V3_LADDER; break;
                case 5: option = ARENA_5V5_LADDER; break;
            }
            return option;
        }
        
        std::string raceToString(uint8 race) {
            std::string race_s = "Inconnu";
            switch (race)
            {
                case RACE_HUMAN:            race_s = "Humain";       break;
                case RACE_ORC:              race_s = "Orc";         break;
                case RACE_DWARF:            race_s = "Dwarf";       break;
                case RACE_NIGHTELF:         race_s = "Elfes de Nuit";   break;
                case RACE_UNDEAD_PLAYER:    race_s = "Mort Vivant";      break;
                case RACE_TAUREN:           race_s = "Tauren";      break;
                case RACE_GNOME:            race_s = "Gnome";       break;
                case RACE_TROLL:            race_s = "Troll";       break;
                case RACE_BLOODELF:         race_s = "Elfes de Sang";   break;
                case RACE_DRAENEI:          race_s = "Draenei";     break;
            }
            return race_s;
        }
        
        std::string classToString(uint8 Class) {
            std::string Class_s = "Inconnu";
            switch (Class)
            {
                case CLASS_WARRIOR:         Class_s = "Guerrier";        break;
                case CLASS_PALADIN:         Class_s = "Paladin";        break;
                case CLASS_HUNTER:          Class_s = "Chasseur";         break;
                case CLASS_ROGUE:           Class_s = "Voleur";          break;
                case CLASS_PRIEST:          Class_s = "Pr�tre";         break;
                case CLASS_DEATH_KNIGHT:    Class_s = "Chevalier de la Mort";   break;
                case CLASS_SHAMAN:          Class_s = "Chaman";         break;
                case CLASS_MAGE:            Class_s = "Mage";           break;
                case CLASS_WARLOCK:         Class_s = "Demoniste";        break;
                case CLASS_DRUID:           Class_s = "Druide";          break;
            }
            return Class_s;
        }
        
        std::string getPlayerStatus(uint32 guid) {
            Player *player = sObjectAccessor->FindPlayer(guid);
            if(!player)
                return "Hors Ligne";
            if(player->isAFK()) 
                return "En Jeu, <AFK> ";
            if(player->isDND()) 
                return "En Jeu, <Actif> ";
            return "Online";
        }
        
        std::string getWinPercent(uint32 wins, uint32 losses) {
            uint32 totalGames = wins + losses;
            if (totalGames == 0)
                return "0%";
            
            std::stringstream buf;
            uint32 percentage = (wins * 100) / totalGames;
            buf << percentage << "%";
            return buf.str();
        }
        
    public:
        npc_pvp_rating() : CreatureScript("npc_pvp_rating"){}
        
    std::string classtostring(uint16 class_id)
    {
        const char* classStr = NULL;
        switch (class_id)
        {
          //warrior
        case CLASS_WARRIOR: classStr = "|TInterface\\ICONS\\inv_sword_27:20:20:0:0|t";
            break;
          //paladin
        case CLASS_PALADIN: classStr = "|TInterface\\ICONS\\ability_thunderbolt:20:20:0:0|t";
            break;
          //hunter
        case CLASS_HUNTER: classStr = "|TInterface\\ICONS\\inv_weapon_bow_07:20:20:0:0|t";
            break;
          //rogue
        case CLASS_ROGUE: classStr = "|TInterface\\ICONS\\inv_throwingknife_04:20:20:0:0|t";
            break;
          //priest
        case CLASS_PRIEST: classStr = "|TInterface\\ICONS\\inv_staff_30:20:20:0:0|t";
            break;
          //Deathknight
        case CLASS_DEATH_KNIGHT: classStr = "|TInterface\\ICONS\\spell_deathknight_classicon:20:20:0:0|t";
            break;
          //Shaman
        case CLASS_SHAMAN: classStr = "|TInterface\\ICONS\\spell_nature_bloodlust:20:20:0:0|t";
            break;
          //mage
        case CLASS_MAGE: classStr = "|TInterface\\ICONS\\inv_staff_13.jpg:20:20:0:0|t";
            break;
          //Warlock
        case CLASS_WARLOCK: classStr = "|TInterface\\ICONS\\spell_nature_drowsy:20:20:0:0|t";
            break;
          //Druid
        case CLASS_DRUID: classStr = "|TInterface\\ICONS\\Ability_Druid_Maul:20:20:0:0|t";
            break;
        default: classStr = "|TInterface\\ICONS\\Inv_misc_questionmark:20:20:0:0|t";
            break;
        }
        return classStr;
    }

    std::string killstorank(uint8 race, uint32 kills)
    {
        const char* rankStr = NULL;
        // alliance icons [blue]
        if ((race == RACE_HUMAN) || (race == RACE_DWARF) || (race == RACE_NIGHTELF) || (race == RACE_GNOME) || (race == RACE_DRAENEI))
        {
            if (kills < 250) // rank under 250 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_01:20:20:0:0|t";
            else if (kills >= 250 && kills < 500) // rank at 250 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_02:20:20:0:0|t";
            else if (kills >= 500 && kills < 750) // rank at 500 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_03:20:20:0:0|t";
            else if (kills >= 750 && kills < 1000) // rank at 750 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_04:20:20:0:0|t";
            else if (kills >= 1000 && kills < 1250) // rank at 1000 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_05:20:20:0:0|t";
            else if (kills >= 1250 && kills < 1500) // rank at 1250 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_06:20:20:0:0|t";
            else if (kills >= 1500 && kills < 1750) // rank at 1500 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_07:20:20:0:0|t";
            else if (kills >= 1750 && kills < 2000) // rank at 1750 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_08:20:20:0:0|t";
            else if (kills >= 2000 && kills < 2250) // rank at 2000 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_09:20:20:0:0|t";
            else if (kills >= 2250 && kills < 2500) // rank at 2250 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_10:20:20:0:0|t";
            else if (kills >= 2500 && kills < 2750) // rank at 2500 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_11:20:20:0:0|t";
            else if (kills >= 2750 && kills < 3000) // rank at 2750 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_12:20:20:0:0|t";
            else if (kills >= 3000 && kills < 4000) // rank at 3000 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_13:20:20:0:0|t";
            else if (kills >= 4000 && kills < 7000) // rank at 4000 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_14:20:20:0:0|t";
            else if (kills >= 7000 && kills < 10000) // rank at 7000 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_15:20:20:0:0|t";
            else if (kills >= 10000) // rank at 10000+ kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_P_250K:20:20:0:0|t";
            else
                rankStr = "|TInterface\\ICONS\\Inv_misc_questionmark:20:20:0:0|t";
        }
        else // horde icons [red]
        {
            if (kills < 250) // rank under 250 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_01:20:20:0:0|t";
            else if (kills >= 250 && kills < 500) // rank at 250 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_02:20:20:0:0|t";
            else if (kills >= 500 && kills < 750) // rank at 500 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_03:20:20:0:0|t";
            else if (kills >= 750 && kills < 1000) // rank at 750 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_04:20:20:0:0|t";
            else if (kills >= 1000 && kills < 1250) // rank at 1000 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_05:20:20:0:0|t";
            else if (kills >= 1250 && kills < 1500) // rank at 1250 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_06:20:20:0:0|t";
            else if (kills >= 1500 && kills < 1750) // rank at 1500 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_07:20:20:0:0|t";
            else if (kills >= 1750 && kills < 2000) // rank at 1750 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_08:20:20:0:0|t";
            else if (kills >= 2000 && kills < 2250) // rank at 2000 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_09:20:20:0:0|t";
            else if (kills >= 2250 && kills < 2500) // rank at 2250 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_10:20:20:0:0|t";
            else if (kills >= 2500 && kills < 2750) // rank at 2500 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_11:20:20:0:0|t";
            else if (kills >= 2750 && kills < 3000) // rank at 2750 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_12:20:20:0:0|t";
            else if (kills >= 3000 && kills < 4000) // rank at 3000 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_13:20:20:0:0|t";
            else if (kills >= 4000 && kills < 7000) // rank at 4000 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_14:20:20:0:0|t";
            else if (kills >= 7000 && kills < 10000) // rank at 7000 kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_15:20:20:0:0|t";
            else if (kills >= 10000) // rank at 10000+ kills
                rankStr = "|TInterface\\ICONS\\Achievement_PVP_P_250K:20:20:0:0|t";
            else
                rankStr = "|TInterface\\ICONS\\Inv_misc_questionmark:20:20:0:0|t";
        }
        return rankStr;
    }
        
    bool OnGossipHello(Player *player, Creature *creature) {
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Top Arene 1c1", GOSSIP_SENDER_MAIN, ARENA_5V5_LADDER);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Top Arene 2c2", GOSSIP_SENDER_MAIN, ARENA_2V2_LADDER);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Top Arene 3c3", GOSSIP_SENDER_MAIN, ARENA_3V3_LADDER);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Top 10 VH", GOSSIP_SENDER_MAIN, TOP_VH);
        player->SEND_GOSSIP_MENU(ARENA_GOSSIP_HELLO, creature->GetGUID());
            
        return true;
    }
        
    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*uiSender*/, uint32 uiAction) 
    {
        player->PlayerTalkClass->ClearMenus();
            
        switch(uiAction) {
            case ARENA_GOODBYE:
            {
                player->PlayerTalkClass->SendCloseGossip();
                break;
            }

            case ARENA_2V2_LADDER:
            case ARENA_5V5_LADDER:
            case ARENA_3V3_LADDER:
            {
                uint32 teamType = optionToTeamType(uiAction);
                QueryResult result = CharacterDatabase.PQuery(
                        "SELECT arenaTeamId, name, rating, seasonWins, seasonGames - seasonWins "
                        "FROM arena_team WHERE type = '%u' ORDER BY rating DESC LIMIT %u;", teamType, ARENA_MAX_RESULTS
                );
                    
                if(!result) {
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Aurevoir", GOSSIP_SENDER_MAIN, ARENA_GOODBYE);
                    player->SEND_GOSSIP_MENU(ARENA_GOSSIP_NOTEAMS, creature->GetGUID());
                } 
                else 
                {
                    //uint64 rowCount = result->GetRowCount();
                    std::string name;
                    uint32 teamId, rating, seasonWins, seasonLosses, rank = 1;
                    do {
                        Field *fields = result->Fetch();
                        teamId = fields[0].GetUInt32();
                        name = fields[1].GetString();
                        rating = fields[2].GetUInt32();
                        seasonWins = fields[3].GetUInt32();
                        seasonLosses = fields[4].GetUInt32();
                            
                        std::stringstream buffer;
                        buffer << rank << ". [" << rating << "] " << name;
                        buffer << " (" << seasonWins << "-" << seasonLosses << ")";
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, buffer.str(), GOSSIP_SENDER_MAIN, ARENA_START_TEAM_LOOKUP + teamId);
                            
                        rank++;
                    } while(result->NextRow());
                        
                    player->SEND_GOSSIP_MENU(ARENA_GOSSIP_TEAMS, creature->GetGUID());
                }
                 break;
            }

            case TOP_VH: // top x killers
            {
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "|TInterface\\PaperDollInfoFrame\\UI-GearManager-Undo:24:24:0:0|t |cff6E353A Aurevoir|r", GOSSIP_SENDER_MAIN, ARENA_GOODBYE);

                QueryResult result = CharacterDatabase.Query("SELECT `name`, `race`, `class`, `totalKills` FROM `characters` WHERE `totalKills`>=1 ORDER BY `totalKills` DESC LIMIT 9");
                if(!result)
                {
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\ICONS\\Inv_misc_questionmark:24:24:0:0|t |cffFF0000No Results!|r", GOSSIP_SENDER_MAIN, TOP_VH);
                    player->PlayerTalkClass->SendGossipMenu(68, creature->GetGUID());
                    return false;
                }

                uint16 counter = 0;
                Field * fields = NULL;
                do
                {
                    std::ostringstream result_string;

                    fields = result->Fetch();
                    std::string char_name = fields[0].GetString();
                    uint32 char_race = fields[1].GetUInt32();
                    uint32 char_class = fields[2].GetUInt32();
                    uint32 char_kills = fields[3].GetUInt32();
            
                    counter += 1;
                    result_string << killstorank(char_race, char_kills) << " " << counter << " " << classtostring(char_class) << " Name |cffFF0000[" << char_name.c_str() <<"]|r, Kills:|cffAB00FF " << char_kills << "|r";
            
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, result_string.str().c_str(), GOSSIP_SENDER_MAIN, TOP_VH);
            
                }while(result->NextRow());
                player->PlayerTalkClass->SendGossipMenu(68, creature->GetGUID());
            } 
            break;

            default:
            {
                if (uiAction > ARENA_START_TEAM_LOOKUP) 
                {
                    uint32 teamId = uiAction - ARENA_START_TEAM_LOOKUP;
                        
                        
                    QueryResult result = CharacterDatabase.PQuery(
                        //      0     1       2           3
                        "SELECT name, rating, seasonWins, seasonGames - seasonWins, "
                        //  4      5                     6     7             8
                        "weekWins, weekGames - weekWins, rank, captainGuid , type "
                        "FROM arena_team WHERE arenaTeamId = '%u'", teamId);
                        
                    // Team Introuvable
                    if(!result) 
                    {
                        player->GetSession()->SendNotification("Equipe d'Arene non trouver.");
                        player->PlayerTalkClass->SendCloseGossip();
                        return true;
                    }
                        

                    Field *fields = result->Fetch();
                    std::string name = fields[0].GetString();
                    uint32 rating = fields[1].GetUInt32();
                    uint32 seasonWins = fields[2].GetUInt32();
                    uint32 seasonLosses = fields[3].GetUInt32();
                    uint32 weekWins = fields[4].GetUInt32();
                    uint32 weekLosses = fields[5].GetUInt32();
                    uint32 rank = fields[6].GetUInt32();
                    uint32 captainGuid = fields[7].GetUInt32();
                    uint32 type = fields[8].GetUInt32();
                    uint32 parentOption = teamTypeToOption(type);
                        
                    std::string seasonWinPercentage = getWinPercent(seasonWins, seasonLosses);
                    std::string weekWinPercentage = getWinPercent(weekWins, weekLosses);
                        
                    std::stringstream buf;
                    buf << "Equipe Nom: " << name;
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
                    buf.str("");
                    buf << "Cote; " << rating << " (rank " << rank << ", bracket " << type << "v" << type << ")";
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
                    buf.str("");
                    buf << "Total Week End: " << weekWins << "-" << weekLosses << " (" << weekWinPercentage << " victoire), " << (weekWins + weekLosses) << " Temp de Jeu"; 
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
                    buf.str("");
                    buf << "Total Saison: " << seasonWins << "-" << seasonLosses << " (" << seasonWinPercentage << " victoire), " << (seasonWins + seasonLosses) << " Temp de Jeu"; 
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
                        
                    QueryResult members = CharacterDatabase.PQuery( "SELECT  a.guid, a.personalRating, a.weekWins, a.weekGames - a.weekWins, a.seasonWins, a.seasonGames - a.seasonWins, c.name, c.race, c.class, c.level FROM arena_team_member a LEFT JOIN characters c ON c.guid = a.guid WHERE arenaTeamId = '%u' ORDER BY a.guid = '%u' DESC, a.seasonGames DESC, c.name ASC", teamId, captainGuid);
                    if(!members) {
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Membre d'Equipe no trouver?", GOSSIP_SENDER_MAIN, parentOption);
                    } 
                    else 
                    {
                        uint32 memberPos = 1;
                        uint32 memberCount = members->GetRowCount();
                        uint32 guid, personalRating, level;
                        std::string name, race, Class;
                            
                        buf.str("");
                        buf << memberCount << " team " << ((memberCount == 1) ? "member" : " members") << " Trouver:";
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
                            
                        do 
                        {
                                
                            fields = members->Fetch();
                            guid = fields[0].GetUInt32();
                            personalRating = fields[1].GetUInt32();
                            weekWins= fields[2].GetUInt32();
                            weekLosses = fields[3].GetUInt32();
                            seasonWins = fields[4].GetUInt32();
                            seasonLosses = fields[5].GetUInt32();
                            name = fields[6].GetString();
                            race = raceToString(fields[7].GetUInt8());
                            Class = classToString(fields[8].GetUInt8());
                            level = fields[9].GetUInt32();
                                
                            seasonWinPercentage = getWinPercent(seasonWins, seasonLosses);
                            weekWinPercentage = getWinPercent(weekWins, weekLosses);
                                
                              
                            buf.str(""); 
                            buf << memberPos << ". "; 
                            
                            if (guid == captainGuid) 
                                buf <<  "Capitaine d'Equipe ";
                            
                            buf << name << ", " << getPlayerStatus(guid);
                                
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
                            buf.str("");
                            buf << "Level " << level << " " << race << " " << Class << ", " << personalRating << " cote personnel.";
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
                            buf.str("");
                            buf << "Week End: " << weekWins << "-" << weekLosses << " (" << weekWinPercentage << " victoire), " << (weekWins + weekLosses) << " Temp de Jeu"; 
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
                            buf.str("");
                            buf << "Saison: " << seasonWins << "-" << seasonLosses << " (" << seasonWinPercentage << " victoire), " << (seasonWins + seasonLosses) << " Temp de Jeu"; 
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
                            memberPos++;
                        } 
                        while(members->NextRow());
                    }

                    buf.str("");
                    buf << "Retour a " << type << "v" << type << " cote";
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
                    player->SEND_GOSSIP_MENU(ARENA_GOSSIP_TEAM_LOOKUP, creature->GetGUID());
                }
            }
        }
        return true;
    }
};

class npc_pvp_arena_reset : public CreatureScript
{
    public:
        npc_pvp_arena_reset() : CreatureScript("npc_pvp_arena_reset") { }

        uint16* GetMmr(Player *player)
        {
            uint16 *mmr = new uint16[3];
            for(int x = 0; x < 3; x++)
            {
                if(ArenaTeam *twos = sArenaTeamMgr->GetArenaTeamById(player->GetArenaTeamId(x)))
                    mmr[x] = twos->GetMember(player->GetGUID())->MatchMakerRating;
                else
                    mmr[x] = 0;
            }
            return mmr;
        }

        bool ChangeMmr(Player *player, int slot, int value)
        {
            if(ArenaTeam *team = sArenaTeamMgr->GetArenaTeamById(player->GetArenaTeamId(slot)))
            {
                ArenaTeamMember *member = team->GetMember(player->GetGUID());
                member->MatchMakerRating = value;
                member->ModifyMatchmakerRating(value - (int)member->MatchMakerRating, slot);
                team->SaveToDB();
                return true;
            }
            return false;
        }

        bool OnGossipHello(Player *player, Creature *_creature)
        {
            uint16 *mmr = GetMmr(player);

            if(mmr[0] > 0)
            {
                player->ADD_GOSSIP_ITEM(0, "Reset 2c2 MMR ", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
                if(mmr[0] > 1600)
                    player->ADD_GOSSIP_ITEM(0, "En dessous 2c2 MMR a 1600", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
            }

            if(mmr[1] > 0)
            {
                player->ADD_GOSSIP_ITEM(0, "Reset 3c3 MMR ", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);
                if(mmr[1] > 1500)
                    player->ADD_GOSSIP_ITEM(0, "En dessous 3c3 MMR a 1500", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);
            }

            if(mmr[2] > 0)
            {
                player->ADD_GOSSIP_ITEM(0, "Reset 5v5 MMR ", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 5);
                if(mmr[1] > 1400)
                    player->ADD_GOSSIP_ITEM(0, "En dessous 5c5 MMR a 1400", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 6);
            }

            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Aurevoir", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+7);
            
            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());
            return true;
        }

        bool OnGossipSelect(Player *player, Creature *_creature, uint32 sender, uint32 action)
        {
            if (action < GOSSIP_ACTION_INFO_DEF + 7)
            {
                uint16 *mmr = GetMmr(player);
                switch(action - GOSSIP_ACTION_INFO_DEF)
                {
                    case 1:
                        if(mmr[0] > 0 && player->HasItemCount(19182, 5))
                        {
                            if(ChangeMmr(player, 0, 1500))
                            {
                                player->DestroyItemCount(19182, 5, true);
                                player->SaveToDB();
                            }
                        }
                        break;
                    case 2:
                        if(mmr[0] > 1600)
                        {
                            if(ChangeMmr(player, 0, 2100))
                                player->SaveToDB();
                        }
                        break;
                    case 3:
                        if(mmr[1] > 0 && player->HasItemCount(19182, 10))
                        {
                            if(ChangeMmr(player, 1, 1500))
                            {
                                player->DestroyItemCount(19182, 10, true);
                                player->SaveToDB();
                            }
                        }
                        break;
                    case 4:
                        if(mmr[1] > 1500)
                        {
                            if(ChangeMmr(player, 1, 2000))
                                player->SaveToDB();
                        }
                        break;
                    case 5:
                        if(mmr[2] > 0 && player->HasItemCount(19182, 15))
                        {
                            if(ChangeMmr(player, 2, 1500))
                            {
                                player->DestroyItemCount(19182, 15, true);
                                player->SaveToDB();
                            }
                        }
                        break;
                    case 6:
                        if(mmr[2] > 1400)
                        {
                            if(ChangeMmr(player, 2, 1900))
                                player->SaveToDB();
                        }
                        break;
                    default:
                        break;
                }
            }

            player->CLOSE_GOSSIP_MENU();
            return true;
        }
};

void AddSC_npc_pvp_rating()
{
    new npc_pvp_rating();
    new npc_pvp_arena_reset();
}
