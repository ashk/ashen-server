/*
scripts de teleportations
Avancement : 99%
*/

#include "ScriptPCH.h"
#include "FreeForAllMgr.h"
#include "Group.h"


/* Creature Variable*/
#define CREATURE_TELEPORTER 800000
#define CREATURE_DISPLAY_ID 11686

/* Gossip Menu */
#define GOSSIP_SENDER_TOOL GOSSIP_SENDER_MAIN+1

//Condition a vérifier en toute circonstance
bool StateValid(Player* pPlayer)
{
    //Erreurs impossible d'être utilisée en combat
    if (pPlayer->IsInCombat())
        return false;

    //Impossible d'être utilisée dans un BG ou une arène
    if (pPlayer->InBattleground() || pPlayer->InArena())       
        return false;

    return true;
};

/* Script */
class item_teleporter : public ItemScript
{
public:
    item_teleporter() : ItemScript("item_teleporter") { }    
    
    bool OnUse(Player *pPlayer, Item *pItem, SpellCastTargets const & /*targets*/)
    {
        if(!StateValid(pPlayer))
            return false;
        
        //Verification de la préscence d'un autre téléporteur
        Creature* old_teleport = pPlayer->FindNearestCreature(CREATURE_TELEPORTER,15.0f,true);
        if(old_teleport && old_teleport->GetOwnerGUID()== pPlayer->GetGUIDLow())
            old_teleport->DisappearAndDie();

        // Invocation de la creature
        Creature *teleporter = pPlayer->SummonCreature(CREATURE_TELEPORTER, pPlayer->GetPositionX(), pPlayer->GetPositionY(), pPlayer->GetPositionZ(), 0, TEMPSUMMON_DEAD_DESPAWN, 10000);

        // Fix des flags et du display_id
        teleporter->SetReactState(REACT_PASSIVE);
        teleporter->SetDisplayId(CREATURE_DISPLAY_ID);
        teleporter->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
        teleporter->AddUInt64Value(UNIT_FIELD_SUMMONEDBY, pPlayer->GetGUID());
        teleporter->AI()->DoAction(pPlayer->GetGUIDLow());
        return true;
    }
};

class npc_teleporter : public CreatureScript
{
public:
    npc_teleporter() : CreatureScript("npc_teleporter") { }

    struct npc_teleporterAI : public ScriptedAI
    {
        npc_teleporterAI(Creature *c) : ScriptedAI(c) {Reset();}
        uint32 timer;

        void UpdateAI(uint32 diff)
        {
            //Timer dépassée la créature disparait.
            if(timer <= diff)
                me->DisappearAndDie();
            else 
                timer -= diff;

            //Check if player is around.
            if (Player *player =  ObjectAccessor::FindPlayer(me->GetOwnerGUID()))
                if (!player->IsAlive() || !me->IsWithinDistInMap(player, 5.0f))
                    me->DisappearAndDie();
        }
        
        //Envoie du GossipMenu lors de l'invocation.
        void DoAction(int32 guid)
        {
            Player *pPlayer = ObjectAccessor::FindPlayer(MAKE_NEW_GUID(guid, 0, HIGHGUID_PLAYER));

            if(pPlayer)
            {
                pPlayer->PlayerTalkClass->ClearMenus();

                // Menu VIP
                if(pPlayer->GetSession()->GetAccountId() == 1 || pPlayer->GetSession()->GetAccountId() == 7 || pPlayer->GetSession()->GetAccountId() == 9 || pPlayer->GetSession()->GetAccountId() == 41 || pPlayer->GetSession()->GetAccountId() == 43)
                {
                    pPlayer->ADD_GOSSIP_ITEM(0, "|cffff0000|TInterface\\icons\\Inv_misc_toy_10:30:30:-24:0|tSpecial P'tit Poneeey", GOSSIP_SENDER_TOOL, 3000);
                }

                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B4F08|TInterface\\icons\\Inv_gizmo_02:30:30:-24:0|tGuide & Infos", GOSSIP_SENDER_TOOL, 2100);
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Inv_drink_05:30:30:-24:0|tFort-Cendré (Shop)", GOSSIP_SENDER_MAIN, 2000);

                //FFA
                if (FreeForAll* ffa = sFreeForAllMgr->getActiveFreeForAll())
                {
                    if (!ffa->isFreeForAllArea(pPlayer->GetAreaId()))
                    {
                        std::string zoneffa_name = "|cff1B019B|TInterface\\icons\\Ability_dualwield:30:30:-24:0|tZone FFA :" + ffa->getName();
                        pPlayer->ADD_GOSSIP_ITEM(0, zoneffa_name.c_str(), GOSSIP_SENDER_MAIN, 2001);
                    }
                }

                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Inv_misc_gem_pearl_04:30:30:-24:0|tZone Farming", GOSSIP_SENDER_TOOL, 1006);
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Inv_shield_64:30:30:-24:0|tZone Bashing", GOSSIP_SENDER_TOOL, 1007);
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Inv_misc_bag_08:30:30:-24:0|tQuêtes Journalière", GOSSIP_SENDER_TOOL, 1004);
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Inv_elemental_primal_air:30:30:-24:0|tInstance et Donjons", GOSSIP_SENDER_TOOL, 1005);
                pPlayer->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, me->GetGUID());
            }
            return; 
        }
        
        //Reset
        void Reset()
        {
            timer = 5*MINUTE*IN_MILLISECONDS;
            me->SetDisplayId(CREATURE_DISPLAY_ID);
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
            me->SetReactState(REACT_PASSIVE);
        }
    };

    CreatureAI* GetAI(Creature* pCreature) const {return new npc_teleporterAI(pCreature);}
    
    bool OnGossipHello(Player* pPlayer, Creature* pCreature)
    {
        //Ne peut pas être utiliser par quelqu'un d'autre que celui qui là invoqué
        if(pPlayer->GetUInt64Value(UNIT_FIELD_SUMMONEDBY)!=pPlayer->GetGUIDLow())
            return false;

        pCreature->AI()->DoAction(pPlayer->GetGUIDLow());
            return true;
    }

    bool OnGossipSelect(Player* pPlayer, Creature* pCreature, uint32 uiSender, uint32 uiAction)
    {
        if (uiSender == GOSSIP_SENDER_MAIN)
            SendDefaultMenu_npc_teleporter(pPlayer, pCreature, uiAction);

        if (uiSender == GOSSIP_SENDER_TOOL)
            SendToolMenu_npc_teleporter(pPlayer, pCreature, uiAction);


        return true;
     }

    
    //Menu et actions principale
    void SendDefaultMenu_npc_teleporter(Player* pPlayer, Creature* pCreature, uint32 uiAction)
    {
        if(!StateValid(pPlayer))
        {
            pPlayer->CLOSE_GOSSIP_MENU();
            pCreature->DisappearAndDie();
        }
        
        switch (uiAction)
        {        
            case 1000: //Renvoie du menu
            {
                pCreature->AI()->DoAction(pPlayer->GetGUIDLow());
                return;
            break;
            }
            
            
            case 2000: //Téléportation à la zone d'achat
            {
                pPlayer->CLOSE_GOSSIP_MENU();
                pCreature->DisappearAndDie();
                pPlayer->TeleportTo(1000, 7471.94f, 4745.38f, 55.29f, 0.293485f);
            break;
            }
            
            case 2001: //Téléportation Zone FFA
            {
                pPlayer->CLOSE_GOSSIP_MENU();
                pCreature->DisappearAndDie();
                sFreeForAllMgr->teleportInFFA(pPlayer);
                break;
            }
        }

        pPlayer->CLOSE_GOSSIP_MENU();
        pCreature->DisappearAndDie();
    }

    //Menu et actions Outils
    void SendToolMenu_npc_teleporter(Player* pPlayer, Creature* pCreature, uint32 uiAction)
    {
    
        if(!StateValid(pPlayer))
        {
            pPlayer->CLOSE_GOSSIP_MENU();
            pCreature->DisappearAndDie();
        }
        
        uint32 groupCount = 1;
        if(Group* group = pPlayer->GetGroup())
        {
            groupCount = group->GetMembersCount();
        }
        
        switch (uiAction)
        {    
            case 3000: // Boobs Party
            {
                pPlayer->PlayerTalkClass->ClearMenus();
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Inv_misc_head_clockworkgnome_01:30:30:-24:0|tGive me 1000 Token Fun, |cffff0000biaaaaaatch!", GOSSIP_SENDER_TOOL, 3001);
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Inv_misc_gift_02:30:30:-24:0|tMaka Maka Héhé change mon Name!", GOSSIP_SENDER_TOOL, 3002);
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Inv_misc_gift_01:30:30:-24:0|tPutain de Brebis change ma putain de Race!", GOSSIP_SENDER_TOOL, 3003);
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Inv_misc_gift_03:30:30:-24:0|tPoooooooosey la change Faction Pooooooooosey", GOSSIP_SENDER_TOOL, 3004);
                pPlayer->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, pCreature->GetGUID());
            break;
            }
            
            case 3001: // 1000 Tokens
            {
                pPlayer->CLOSE_GOSSIP_MENU();
                pCreature->DisappearAndDie();
                pPlayer->AddItem(80003, 1000);
                pPlayer->GetSession()->SendAreaTriggerMessage("Matte ton sac boulet, tu viens de recevoir 1000 Tokens pour une pipe 6 francs!");
            break;
            }
            
            case 3002: // Change Name
            {
                pPlayer->CLOSE_GOSSIP_MENU();
                pCreature->DisappearAndDie();
                pPlayer->SetAtLoginFlag(AT_LOGIN_RENAME);
                pPlayer->GetSession()->SendAreaTriggerMessage("Déco toi pour passer en mode Somalien qui vient de voler une identité, c'est POSEYY!!");
            break;
            }
            
            case 3003: // Change Race
            {
                pPlayer->CLOSE_GOSSIP_MENU();
                pCreature->DisappearAndDie();
                pPlayer->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
                pPlayer->GetSession()->SendAreaTriggerMessage("T'en avais vraiment marre de ta race de paysan? Déco toi et tu produiras du bon lait de brebis hmmm Poseyyyyy!");
            break;
            }
            
            case 3004: // Change Race
            {
                pPlayer->CLOSE_GOSSIP_MENU();
                pCreature->DisappearAndDie();
                pPlayer->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
                pPlayer->GetSession()->SendAreaTriggerMessage("Toi t'es posseeeeeeeeeydeyyy, allez casse toi de cette faction on veut plus de toi ici, a ta prochaine déco t'es plus des notre sale traître!");
            break;
            }
            
            case 2100: // Guide & Infos
            {
                pPlayer->PlayerTalkClass->ClearMenus();
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(0, "La Zone Marchande : Fort Cendré", 2100, 0, "Plus communément appelée la zone marchande, ou bien Fort Cendré pour les intimes, cette zone est la première que vous visiterez à votre arrivée sur Ashen-Server. $BAvant tout, cette Zone est celle où vous retrouverez tous les objets et fonctionnalités requis aux joueurs pour partir à l\'aventure. $BDes équipements jusqu\'au coiffeur en passant par vos métiers, tout est censé s\'y trouver !$B$BDans cette Zone nous avons essayé de créer un aspect Role-Play ,fort Cendré étant le pilier historique d\'Ashen, plusieurs quêtes et suites de quête font souvent référence à cette Zone, ainsi que des micros évents.$B", 0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(0, "L'expérience Ashen !", 2100, 0, "Sur Ashen Server les Instances, et les autres activités n\'ont pas été pensées comme sur tous les autres Serveurs ! $B$BIci nous essayons de retreinte au maximum le Power-Leveling et toutes autres possibilités d\'avancer trop rapidement dans l\'aventure, afin de mettre tous les joueurs sur un même pied d\'égalité !$B$BComment fonctionne ce système ? Quand vous arrivez sur le serveur, votre principal objectif est d\'avancer dans le monde d\'Ashen. $BEn servant votre ville, Fort Cendré, par l’intermédiaire de quêtes vous recevrez des divers monnaies afin de vous acheter des pièces d\'équipements, des montures et tout autres sortes d\'objets utiles à votre aventure. $B$BMais le plus important ici reste la quête principale qui au fur et à mesure que vous la complété vous permet à la fois d\'avancer dans l\'aventure et de gagner des monnaies afin d\'obtenir de nouveaux objets et sorts ! $BEn avançant dans l\'aventure, cela vous permettra d\'obtenir les accès aux Instances supérieures, et à certaines zones exclusives comme la Zone Farming !", 0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(0, "Le PvP sur Ashen ?", 2100, 0, "Vous voulez signaler une erreur ? Faire un rapport ", 0, false);
                pPlayer->ADD_GOSSIP_ITEM_EXTENDED(0, "Les Events", 2100, 0, "Vous voulez signaler une erreur ? Faire un rapport ", 0, false);
                pPlayer->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, pCreature->GetGUID());
            break;
            }
            
            case 1004: // Quêtes Journalière Sub
            {
                pPlayer->PlayerTalkClass->ClearMenus();
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Achievement_zone_grizzlyhills_06:30:30:-24:0|tCampement des Tuskars", GOSSIP_SENDER_TOOL, 7000);
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Achievement_zone_elwynnforest:30:30:-24:0|tLa Ferme de Kent le Fermier", GOSSIP_SENDER_TOOL, 7001);
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Achievement_zone_boreantundra_02:30:30:-24:0|tLe Coin Caché de Gazix", GOSSIP_SENDER_TOOL, 7002);
                pPlayer->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, pCreature->GetGUID());
            break;
            }
            
            case 1005: // Instance et Donjons Sub
            {

                pPlayer->PlayerTalkClass->ClearMenus();
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Achievement_zone_blackrock_01:30:30:-24:0|tI1 : RageFeu |cffff0000(1 joueur)", GOSSIP_SENDER_TOOL, 7003);
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Achievement_zone_bladesedgemtns_01:30:30:-24:0|tI2 : Remparts des Flammes |cffff0000(2 joueurs)", GOSSIP_SENDER_TOOL, 7004);
                pPlayer->ADD_GOSSIP_ITEM(0, "|cff1B019B|TInterface\\icons\\Achievement_zone_deadwindpass:30:30:-24:0|tI3 : La Forge des Ames |cffff0000(3 joueurs)", GOSSIP_SENDER_TOOL, 7005);
                pPlayer->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, pCreature->GetGUID());
            break;
            }
            
            case 1006: // Zone Farming
            {    
                if (pPlayer->GetQuestStatus(50014) == 6 || pPlayer->GetQuestStatus(50014) == 1)
                {
                    pPlayer->CLOSE_GOSSIP_MENU();
                    pCreature->DisappearAndDie();
                    pPlayer->TeleportTo(0, -14546.5f, -82.44f, 8.709879f, 4.666570f);
                }
                else
                {
                    pPlayer->GetSession()->SendAreaTriggerMessage("Vous devez finir l'instance I2 avant de pouvoir accéder à la Zone Farming");
                    pPlayer->CLOSE_GOSSIP_MENU();
                }
            break;
            }
            
            case 1007: // Zone Bashing
            {    
                if (pPlayer->GetQuestStatus(50014) == 6 || pPlayer->GetQuestStatus(50014) == 1)
                {
                    pPlayer->CLOSE_GOSSIP_MENU();
                    pCreature->DisappearAndDie();
                    pPlayer->TeleportTo(0, -14280.029297f, 427.195160f, 35.020054f, 2.580554f);
                }
                else
                {
                    pPlayer->GetSession()->SendAreaTriggerMessage("Vous devez finir l'instance I2 avant de pouvoir accéder à la Zone Bashing");
                    pPlayer->CLOSE_GOSSIP_MENU();
                }
            break;
            }
            
            case 7000: // Campement Tuskar
            {
                pPlayer->CLOSE_GOSSIP_MENU();
                pCreature->DisappearAndDie();
                pPlayer->TeleportTo(1000, 7811.71f, 5047.49f, 3.236755f, 3.231245f);
            break;
            }

            case 7001: // Kent
            {
                pPlayer->CLOSE_GOSSIP_MENU();
                pCreature->DisappearAndDie();
                pPlayer->TeleportTo(1000, 7926.79f, 4903.56f, 1.83f, 2.336671f);
            break;
            }
            
            case 7002: // Gazix le Fou
            {
                pPlayer->CLOSE_GOSSIP_MENU();
                pCreature->DisappearAndDie();
                pPlayer->TeleportTo(1000, 7564.49f, 4654.68f, 1.11f, 0.491860f);
            break;
            }
            
            case 7003: // Rage feu
            {
                if (pPlayer->GetQuestStatus(50004) == 6 || pPlayer->GetQuestStatus(50004) == 1)
                {
                    if (pPlayer->GetQuestStatus(60000) != 6)
                    {
                        pPlayer->GetSession()->SendAreaTriggerMessage("Faites la quête de départ avant de partir à la guerre !");
                        pPlayer->CLOSE_GOSSIP_MENU();    
                    }
                    else
                    {
                        if (pPlayer->GetQuestStatus(50013) != 6)
                        {
                        
                            if (groupCount >= 2)
                            {
                                pPlayer->GetSession()->SendAreaTriggerMessage("Vous devez être seul pour faire cette instance!");
                                
                                pPlayer->CLOSE_GOSSIP_MENU();
                            }
                            else        
                            {
                                pPlayer->CLOSE_GOSSIP_MENU();
                                pCreature->DisappearAndDie();
                                pPlayer->TeleportTo(389, 0.01f, -23.78f, -20.56f, 4.341724f);
                            }
                        }
                        else
                        {
                            pPlayer->GetSession()->SendAreaTriggerMessage("Vous avez déjà fini cette instance.");
                            pPlayer->CLOSE_GOSSIP_MENU();
                        }
                    }
                }
                else
                {
                    pPlayer->GetSession()->SendAreaTriggerMessage("Allez voir Ashk le Roi de la Quête avant de partir à l'aventure !");
                    pPlayer->CLOSE_GOSSIP_MENU();    
                }
            break;
            }    
            
            case 7004: // Flammes Infernales
            {
                if (pPlayer->GetQuestStatus(50013) == 6 || pPlayer->GetQuestStatus(50013) == 1)
                {
                    /*if (pPlayer->GetQuestStatus(50014) != 6)
                    {*/
                        if (groupCount >= 3)
                        {
                            pPlayer->GetSession()->SendAreaTriggerMessage("Vous devez être au maximum 2 pour faire cette instance");
                            pPlayer->CLOSE_GOSSIP_MENU();
                        }
                        else        
                        {
                            pPlayer->CLOSE_GOSSIP_MENU();
                            pCreature->DisappearAndDie();
                            pPlayer->TeleportTo(543, -1353.74f, 1644.75f, 68.41f, 1.179209f);
                        }
                    //}
                    //else
                    //{
                        //pPlayer->GetSession()->SendAreaTriggerMessage("Vous avez déjà fini cette instance.");
                        //pPlayer->CLOSE_GOSSIP_MENU();
                    //}
                }
                else
                {
                    pPlayer->GetSession()->SendAreaTriggerMessage("Vous devez finir l'instance I1 avant de pouvoir faire cette instance");
                    pPlayer->CLOSE_GOSSIP_MENU();
                }    
            break;
            }
            
            case 7005: // Forge des Ames
            {    
                //if (pPlayer->GetQuestStatus(50012) != 6)
                //{

                
                    if (pPlayer->GetQuestStatus(50014) == 6 || pPlayer->GetQuestStatus(50014) == 1)
                    {
                        if (groupCount >= 4)
                        {
                            pPlayer->GetSession()->SendAreaTriggerMessage("Vous ne pouvez être plus de 2 pour faire cette instance!");
                            pPlayer->CLOSE_GOSSIP_MENU();
                        }
                        else        
                        {
                            pPlayer->CLOSE_GOSSIP_MENU();
                            pCreature->DisappearAndDie();
                            pPlayer->TeleportTo(632, 4919.95f, 2180.88f, 638.73f, 2.011229f);
                        }
                    }
                    else
                    {
                        pPlayer->GetSession()->SendAreaTriggerMessage("Vous devez finir l'instance I2 avant de pouvoir faire cette instance");
                        pPlayer->CLOSE_GOSSIP_MENU();
                    }
                //}
                //else
                //{
                        //pPlayer->GetSession()->SendAreaTriggerMessage("Vous avez déjà fini cette instance.");
                        //pPlayer->CLOSE_GOSSIP_MENU();
                //}
            break;
            }
        }
    }
};


void AddSC_item_teleporter()
{
    new item_teleporter;
    new npc_teleporter;
}