#include "ScriptPCH.h"

class player_resetduel_script : public PlayerScript
{
public:
    player_resetduel_script() : PlayerScript("player_resetduel_script"){}

    void OnDuelStart(Player* player1, Player* player2)
    {
        //Remove all cooldown
        player1->RemoveAllSpellCooldown();
        player2->RemoveAllSpellCooldown();

        //Set power and health at max p1
        player1->SetHealth(player1->GetMaxHealth());
        if (player1->getPowerType() == POWER_MANA)
            player1->SetPower(POWER_MANA, player1->GetMaxPower(POWER_MANA));

        //Set power and health at max p2
        player2->SetHealth(player2->GetMaxHealth());
        if (player2->getPowerType() == POWER_MANA)
            player2->SetPower(POWER_MANA, player2->GetMaxPower(POWER_MANA));
    }
    
    void OnDuelEnd(Player* winner, Player* loser, DuelCompleteType)
    {

        winner->RemoveAllSpellCooldown();
        loser->RemoveAllSpellCooldown();

        winner->SetHealth(winner->GetMaxHealth());
        if (winner->getPowerType() == POWER_MANA)
            winner->SetPower(POWER_MANA, winner->GetMaxPower(POWER_MANA));
        
        loser->SetHealth(loser->GetMaxHealth());
        if (loser->getPowerType() == POWER_MANA)
            loser->SetPower(POWER_MANA, loser->GetMaxPower(POWER_MANA));
    }
};
           
void ADDSC_player_resetduel_script()
{
    new player_resetduel_script();
}