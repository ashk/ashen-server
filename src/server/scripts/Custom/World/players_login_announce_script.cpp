#include "ScriptPCH.h"

class players_login_announce_script : public PlayerScript
{
public:
    players_login_announce_script() : PlayerScript("players_login_announce_script"){}

    void OnLogin(Player* player)
    {
        player->GetSession()->SendAreaTriggerMessage("|TInterface\\icons\\Inv_misc_rune_01:35:35:-24:0|t Utilise ta Pierre de Téléportation dans ton sac pour te téléporter !");
    }
};
           
void ADDSC_players_login_announce_script()
{
    new players_login_announce_script();
}