#include "ScriptPCH.h"
#include "Group.h"

class players_creature_reward_script : public PlayerScript
{
public:
    players_creature_reward_script() : PlayerScript("players_creature_reward_script"){}

    void OnCreatureKill(Player* killer, Creature* killed) 
    {
        uint32 honor = 0;
        
        if(killed)
        {
            //Check des creatures
            switch(killed->GetEntry())
            {
                case 999914:
                case 999915:
                case 999916:
                    honor = 30;
                    break;
                
                case 999917:
                case 999918:
                case 999919:
                    honor = 300;
                    break;
                    
                case 999950:
                    honor = 1;
                    break;
            }
        }
 
        //Reward honor
        if (honor > 0)
        {
            WorldPacket data(SMSG_PVP_CREDIT, 4 + 8 + 4);
            data << uint32(honor);
            data << uint64(0);
            data << uint32(0);

            killer->GetSession()->SendPacket(&data);
            killer->ModifyHonorPoints(honor);

            //Si on est dans un group tout le group doit win
            Group* group = killer->GetGroup();
            if (group)
            {
              for (Group::member_citerator itr = group->GetMemberSlots().begin(); itr != group->GetMemberSlots().end(); ++itr)
              {
                    if ((*itr).guid == killer->GetGUID())
                      continue;

                    if (Player* player = ObjectAccessor::FindPlayer((*itr).guid))
                    {
                        if(player->GetAreaId() != killer->GetAreaId())
                            continue;
                            
                        player->GetSession()->SendPacket(&data);
                        player->ModifyHonorPoints(honor);
                    }
               } 
            }
        }
    }
};
           
void ADDSC_players_creature_reward_script()
{
    new players_creature_reward_script();
}