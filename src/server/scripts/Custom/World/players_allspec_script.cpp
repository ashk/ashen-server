#include "ScriptPCH.h"

class players_allspec_script : public PlayerScript
{
public:
    players_allspec_script() : PlayerScript("players_allspec_script"){}

    void OnCreate(Player* player)
    {
        for (uint32 i = 0; i < sTalentStore.GetNumRows(); ++i)
        {
            TalentEntry const* talentInfo = sTalentStore.LookupEntry(i);
            if (!talentInfo)
                continue;

            TalentTabEntry const* talentTabInfo = sTalentTabStore.LookupEntry(talentInfo->TalentTab);
            if (!talentTabInfo)
                continue;

            uint32 classMask = player->getClassMask();
            if ((classMask & talentTabInfo->ClassMask) == 0)
                continue;

            // On cherche le rang maximum.
            uint32 spellId = 0;
            for (int8 rank = MAX_TALENT_RANK - 1; rank >= 0; --rank)
            {
                if (talentInfo->RankID[rank] != 0)
                {
                    spellId = talentInfo->RankID[rank];
                    break;
                }
            }

            if (!spellId)
                continue;

            SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellId);
            if (!spellInfo || !SpellMgr::IsSpellValid(spellInfo, player->GetSession()->GetPlayer(), false))
                continue;

            // On lui ajoute les sorts de rang maximum.
            player->learnSpellHighRank(spellId);
            player->AddTalent(spellId, player->GetActiveSpec(), true);
        }
        player->SetFreeTalentPoints(0);
        player->SaveToDB();
    }
};
           
void ADDSC_players_allspec_script()
{
    new players_allspec_script();
}