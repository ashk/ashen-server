/*
Ashen Server
*/

#include "ScriptPCH.h"
#include "CustomEventMgr.h"
#include "CustomEvent.h"
#include "World.h"

class customevent_commandscript : public CommandScript
{
public:
    customevent_commandscript() : CommandScript(" customevent_commandscript") { }

    ChatCommand* GetCommands() const OVERRIDE
    {
        static ChatCommand customeventCommandTable[] =
        {
            { "start", rbac::RBAC_PERM_COMMAND_ASHEN_GM, false, &HandleCustomEventStartCommand, "", NULL },
            { "stop", rbac::RBAC_PERM_COMMAND_ASHEN_GM, false, &HandleCustomEventStopCommand, "", NULL },
            { "list", rbac::RBAC_PERM_COMMAND_ASHEN_GM, true, &HandleCustomEventListCommand, "", NULL },
        };

        static ChatCommand commandTable[] =
        {
            { "customevent", rbac::RBAC_PERM_COMMAND_ASHEN_GM, true, NULL, "", customeventCommandTable },
            { NULL, 0, false, NULL, "", NULL }
        };
        return commandTable;
    }

    static bool HandleCustomEventStartCommand(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        uint32 eventId = atoi(args);

        //Pas un id
        if (eventId <= 0)
            return false;

        //Mauvaise id
        if (!sCustomEventMgr->isCustomEvent(eventId))
            return false;

        //Démarrage de l'event
        CustomEvent* oEvent = sCustomEventMgr->getCustomEvent(eventId);
        oEvent->setState(true);
        
        //Annonce
        std::string announce = "|cffffff00L'événement " + oEvent->getName() +" est démarré !|r";
        
        WorldPacket data(SMSG_NOTIFICATION, (announce.size() + 1));
        data << announce;
        sWorld->SendGlobalMessage(&data);

        return true;
    }

    static bool HandleCustomEventStopCommand(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        uint32 eventId = atoi(args);

        //Pas un id
        if (eventId <= 0)
            return false;

        //Mauvaise id
        if (!sCustomEventMgr->isCustomEvent(eventId))
            return false;

        //Arret de l'event
        CustomEvent* oEvent = sCustomEventMgr->getCustomEvent(eventId);
        oEvent->setState(false);

        //Annonce
        std::string announce = "|cffffff00L'événement " + oEvent->getName() + " est arreté !|r";

        WorldPacket data(SMSG_NOTIFICATION, (announce.size() + 1));
        data << announce;
        sWorld->SendGlobalMessage(&data);

        return true;
    }

    static bool HandleCustomEventListCommand(ChatHandler* handler, char const* args)
    {
        std::map<uint32, CustomEvent*> customEventList = sCustomEventMgr->getCustomEventList();

        std::string std_actif = "|cff09AD03Actif|r";
        std::string std_inactif = "|cffAD0303Inactif|r";

        for (std::map<uint32, CustomEvent*>::iterator itr = customEventList.begin(); itr != customEventList.end(); ++itr)
        {
            CustomEvent* customEvent = itr->second;
            std::string text = std::to_string(customEvent->getId()) + " - " + customEvent->getName() + " - " + ((customEvent->getState()) ? std_actif : std_inactif);
            handler->PSendSysMessage(text.c_str());
        }

        return true;
    }
};


void AddSC_customevent_commandscript()
{
    new customevent_commandscript();
}