SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `custom_event_template`;
CREATE TABLE `custom_event_template` (
  `entry` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `eventEntry` int(10) unsigned NOT NULL DEFAULT '0',
  `areaId` text,
  `name` text,
  `default_map` int(11) DEFAULT NULL,
  `default_x` float(11,0) DEFAULT NULL,
  `default_y` float(11,0) DEFAULT NULL,
  `default_z` float(11,0) DEFAULT NULL,
  `default_orientation` float(11,0) DEFAULT NULL,
  PRIMARY KEY (`entry`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;