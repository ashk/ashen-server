CREATE TABLE `area_allowed` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `areaId` int(10) unsigned DEFAULT NULL,
  `note` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
