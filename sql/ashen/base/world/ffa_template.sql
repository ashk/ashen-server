CREATE TABLE `ffa_template` (
  `entry` mediumint(11) NOT NULL AUTO_INCREMENT COMMENT 'Entry',
  `name` text NOT NULL COMMENT 'Name',
  `areaId` text NOT NULL COMMENT 'AreaId Separator ";"',
  PRIMARY KEY (`entry`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
