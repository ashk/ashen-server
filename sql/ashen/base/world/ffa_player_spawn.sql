CREATE TABLE `ffa_player_spawn` (
  `entry` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ffa_entry` int(11) unsigned NOT NULL DEFAULT '0',
  `mapId` int(11) unsigned DEFAULT NULL,
  `position_x` float DEFAULT NULL,
  `position_y` float DEFAULT NULL,
  `position_z` float DEFAULT NULL,
  `orientation` float DEFAULT NULL,
  PRIMARY KEY (`entry`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
