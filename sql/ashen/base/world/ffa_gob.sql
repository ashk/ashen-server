CREATE TABLE `ffa_gob` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gob_guid` int(11) NOT NULL,
  `ffa_entry` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`,`gob_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

