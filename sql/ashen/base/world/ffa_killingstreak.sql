CREATE TABLE `ffa_killingstreak` (
  `entry` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `nb_kill` mediumint(11) NOT NULL,
  `rew_honor` mediumint(11) unsigned NOT NULL,
  `rew_arena` mediumint(11) unsigned NOT NULL,
  `rew_item` mediumint(11) unsigned NOT NULL,
  `count` mediumint(11) unsigned NOT NULL,
  `sound_id` mediumint(11) unsigned NOT NULL,
  `announce` longtext NOT NULL,
  PRIMARY KEY (`entry`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;