CREATE TABLE `ffa_creature` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ffa_entry` int(11) unsigned DEFAULT NULL,
  `creature_guid` int(11) NOT NULL,
  PRIMARY KEY (`id`,`creature_guid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;